/*
 * Copyright (c) 2012 Michael Beuse <mbeuse@techfak.uni-bielefeld.de>
 *
 * This file is part of PQuery.
 *
 * PQuery is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * PQuery is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with PQuery.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $Id$
 */
#ifndef _PQ_XQNODE_HPP_
#define _PQ_XQNODE_HPP_ 1

#include <pquery/AtomicValue.hpp>

#include <xqilla/items/Node.hpp>

// FIXME: just included for tracing method calls.
// DEBUG_START
#include <iostream>
//static void traceXQCall(const char *fn, const char *file, unsigned int line)
//{
//  ::std::cout << "DEBUG:" << file << ":" << line << ":" << fn << " called" << ::std::endl;
//}
//#define XQCALL traceXQCall(__PRETTY_FUNCTION__, __FILE__, __LINE__)
#define XQCALL
// DEBUG_END

namespace google {
namespace protobuf {
  class Message;
  class Descriptor;
  class FieldDescriptor;
  namespace io {
    class ZeroCopyOutputStream;
  }
}
}

namespace pquery
{

/**
 * Main class for PQuery and XQilla interoperation.
 */
class XQNode : public Node
{
  public:
    /// Reference counted pointer type.
    typedef RefCountPointer<const XQNode> Ptr;

    /// The "XQNode" interface name.
    static const XMLCh xqnode_string[];

    /// Virtual destructor.
    virtual ~XQNode() {}

    /**
     * @name XQilla API integration
     */
    ///@{
    /**
     * Methods offered by XQilla API.
     */

    virtual const XMLCh *asString(const DynamicContext *context) const = 0;
    virtual const XMLCh *getTypeURI() const = 0;
    virtual const XMLCh *getTypeName() const = 0;
    virtual void generateEvents(EventHandler *events, const DynamicContext *context,
                                bool preserveNS = true, bool preserveType = true) const = 0;
    virtual void *getInterface(const XMLCh *name) const = 0;
    virtual bool hasInstanceOfType(const XMLCh *typeURI, const XMLCh *typeName, const DynamicContext *context) const = 0;
    /// Unsupported by PQuery. Always returns empty sequence.
    virtual Sequence dmBaseURI(const DynamicContext *context) const;
    virtual const XMLCh *dmNodeKind() const = 0;
    virtual ATQNameOrDerived::Ptr dmNodeName(const DynamicContext *context) const = 0;
    virtual const XMLCh *dmStringValue(const DynamicContext *context) const = 0;
    virtual Sequence dmTypedValue(DynamicContext *context) const = 0;
    /// Unsupported by PQuery. Always returns empty sequence.
    virtual Sequence dmDocumentURI(const DynamicContext *context) const;
    virtual ATQNameOrDerived::Ptr dmTypeName(const DynamicContext *context) const = 0;
    virtual ATBooleanOrDerived::Ptr dmNilled(const DynamicContext *context) const = 0;
    virtual bool lessThan(const Node::Ptr &other, const DynamicContext *context) const = 0;
    virtual bool equals(const Node::Ptr &other) const = 0;
    virtual bool uniqueLessThan(const Node::Ptr &other, const DynamicContext *context) const = 0;
    virtual Node::Ptr root(const DynamicContext *context) const = 0;
    virtual Node::Ptr dmParent(const DynamicContext *context) const = 0;
    virtual ::Result dmAttributes(const DynamicContext *context, const LocationInfo *info) const = 0;
    virtual ::Result dmNamespaceNodes(const DynamicContext *context, const LocationInfo *info) const = 0;
    virtual ::Result dmChildren(const DynamicContext *context, const LocationInfo *info) const = 0;
    virtual ::Result getAxisResult(XQStep::Axis axis, const NodeTest *nodeTest,
                                 const DynamicContext *context, const LocationInfo *info) const = 0;
    virtual ATBooleanOrDerived::Ptr dmIsId(const DynamicContext *context) const = 0;
    virtual ATBooleanOrDerived::Ptr dmIsIdRefs(const DynamicContext *context) const = 0;

    ///@}

    /**
     * @name PQuery extensions
     */
    ///@{

    /**
     * Returns the message descriptor, if this node is a message field or the root
     * message. Otherwise returns <code>NULL</code>.
     *
     * @return the message descriptor or <code>NULL</code> if no message field.
     */
    virtual const PQ_PROTOBUF_NS Descriptor *getDescriptor() const = 0;

    /**
     * Returns the field descriptor, if this node is a field. Otherwise returns
     * <code>NULL</code>.
     *
     * @return the field descriptor or <code>NULL</code> if not a field.
     */
    virtual const PQ_PROTOBUF_NS FieldDescriptor *getFieldDescriptor() const = 0;

    /**
     * Stores the field value in <code>value</code> and return <code>true</code>.
     * If this node doesn't have an atomic value don't change <code>value</code>
     * and return <code>false</code>.
     *
     * @param [out] value the field value.
     * @return <code>true</code> if <code>value</code> holds field value otherwise
     *         <code>false</code>.
     */
    virtual bool getValue(AtomicValue &value) const = 0;

    /**
     * Returns a Protocol Buffer message object. If this field cannot be represented
     * by a Protocol Buffer message object returns <code>NULL</code>.
     *
     * @note Object ownership is passed to the caller.
     *
     * @return the corresponding Protocol Buffer object or <code>NULL</code> if
     *         not representable.
     */
    virtual PQ_PROTOBUF_NS Message *getMessage() const = 0;

    /**
     * Serializes the node to the specified stream and return <code>true</code>.
     * If the node cannot be serialized return <code>false</code>.
     *
     * @param [in] ostream the output stream.
     * @return <code>true</code> if successfully serialized otherwise <code>false</code>.
     */
    virtual bool serialize(PQ_PROTOBUF_NS io::ZeroCopyOutputStream *ostream) const = 0;

    /**
     * Serializes the node into the supplied string and returns <code>true</code>.
     * If the node cannot be serialized returns <code>false</code>.
     *
     * @param [out] str the string to serialize into.
     * @return <code>true</code> if successfully serialized otherwise <code>false</code>.
     */
    virtual bool serialize(::std::string &str) const = 0;

    ///@}
};

}

#endif /* !_PQ_XQNODE_HPP_ */
