/*
 * Copyright (c) 2012 Michael Beuse <mbeuse@techfak.uni-bielefeld.de>
 *
 * This file is part of PQuery.
 *
 * PQuery is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * PQuery is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with PQuery.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $Id$
 */
#ifndef _PQ_PQNODE_HPP_
#define _PQ_PQNODE_HPP_ 1

#include <pquery/AtomicValue.hpp>

namespace pquery
{
namespace io
{

using namespace ::google::protobuf;

class PQNode
{
  public:
    typedef ::boost::shared_ptr<const PQNode> Ptr;

    enum NodeType
    {
      ROOT,
      MESSAGE,
      FIELD
    };

    virtual ~PQNode() {}

    virtual void *getInterface(const char *name) const = 0;

    virtual NodeType getType() const = 0;

    virtual PQNode::Ptr root() const = 0;
    virtual PQNode::Ptr parent() const = 0;
    virtual PQNode::Ptr firstChild() const = 0;
    virtual PQNode::Ptr nextSibling() const = 0;
    virtual PQNode::Ptr previousSibling() const = 0;

    virtual bool getValue(AtomicValue &value) const = 0;

    virtual const Descriptor *getDescriptor() const = 0;
    virtual const FieldDescriptor *getFieldDescriptor() const = 0;

    virtual bool equals(const PQNode::Ptr &other) const = 0;

    virtual std::string DebugString() const = 0;
};

class PQNodeFactory
{
  public:
    virtual ~PQNodeFactory() {}

    virtual PQNode::Ptr createNode(const Descriptor *desc, ::std::string *str) = 0;

    virtual PQNode::Ptr createNode(Message *message) const = 0;
};

}
}

#endif /* !_PQ_PQNODE_HPP_ */
