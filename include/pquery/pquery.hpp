/*
 * Copyright (c) 2012 Michael Beuse <mbeuse@techfak.uni-bielefeld.de>
 *
 * This file is part of PQuery.
 *
 * PQuery is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * PQuery is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with PQuery.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $Id$
 */
#ifndef _PQ_PQUERY_HPP_
#define _PQ_PQUERY_HPP_ 1

#include <pquery/PQNode.hpp>

namespace google {
namespace protobuf {
class Descriptor;
}
}

namespace pquery
{

class XQStringPool;

class Item
{
  public:
    typedef ::boost::shared_ptr<Item> Ptr;

    enum Type
    {
      MESSAGE, FIELD, ATOMIC
    };

    virtual ~Item() {}

    virtual void *getInterface(const char *name) const = 0;

    virtual Type getType() const = 0;

    virtual std::string asString() const = 0;
};

class Message : public virtual Item
{
  public:
    typedef ::boost::shared_ptr<Message> Ptr;

    virtual ~Message() {}

    virtual Type getType() const;

    virtual const std::string &getName() const = 0;

    virtual const PQ_PROTOBUF_NS Descriptor *getDescriptor() const = 0;

    virtual PQ_PROTOBUF_NS Message *getMessage() const = 0;

    virtual bool serialize(std::string &str) const = 0;
};

class Field : public virtual Item
{
  public:
    typedef ::boost::shared_ptr<Field> Ptr;

    virtual ~Field() {}

    virtual Type getType() const;

    virtual const std::string &getName() const = 0;

    virtual const PQ_PROTOBUF_NS FieldDescriptor *getDescriptor() const = 0;

    virtual AtomicValue getValue() const = 0;
};

class Atomic : public virtual Item
{
  public:
    typedef ::boost::shared_ptr<Atomic> Ptr;

    virtual ~Atomic() {}

    virtual Type getType() const;

    virtual AtomicValue getValue() const = 0;
};

class ResultImpl
{
  public:
    typedef ::boost::shared_ptr<ResultImpl> Ptr;

    virtual ~ResultImpl() {}

    virtual Item::Ptr next() = 0;
};

class Result
{
  protected:
    ResultImpl::Ptr impl_;

  public:
    Result() : impl_() {}
    Result(const Result &o) : impl_(o.impl_) {}
    Result(const ResultImpl::Ptr &impl) : impl_(impl) {}
    ~Result() {}

    Result &operator=(const Result &o);

    Item::Ptr next();
};

class Query
{
  public:
    typedef ::boost::shared_ptr<Query> Ptr;

    virtual ~Query() {}

    virtual const std::string &getQuery() const = 0;

    virtual Result execute(const Item::Ptr &context) = 0;
};

class PQuery
{
  public:
    PQuery();

    ~PQuery();

    static XQStringPool *fgStringPool;

    Item::Ptr getMessage(const PQ_PROTOBUF_NS Descriptor *descriptor, std::string *bytes);

    Query::Ptr parse(const std::string &xpath);
};

}

#endif /* !_PQ_PQUERY_HPP_ */
