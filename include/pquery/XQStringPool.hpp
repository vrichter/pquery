/*
 * Copyright (c) 2012 Michael Beuse <mbeuse@techfak.uni-bielefeld.de>
 *
 * This file is part of PQuery.
 *
 * PQuery is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * PQuery is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with PQuery.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $Id$
 */
#ifndef _PQ_XQSTRINGPOOL_HPP_
#define _PQ_XQSTRINGPOOL_HPP_ 1

#include <pquery/common.hpp>

#include <utility>
#include <boost/unordered_map.hpp>
#include <xercesc/util/PlatformUtils.hpp>

namespace google {
namespace protobuf {
  class Descriptor;
  class FieldDescriptor;
  class EnumDescriptor;
  class EnumValueDescriptor;
}
}

namespace pquery
{

using namespace ::google::protobuf;

/**
 * Used for transcoding Protocol Buffer descriptors into UTF-16 strings to be
 * used with XQilla.
 */
class XQStringPool
{
  public:
    /// Yields "http://code.cor-lab.de/pquery".
    static const XMLCh fgURI_PQUERY[];
    /// Yields "message".
    static const XMLCh fgMESSAGE[];

  protected:
    typedef ::std::pair<const XMLCh *, const XMLCh *> URIandType;
    typedef ::boost::unordered_map<const void *, URIandType> TypeMap;
    typedef ::boost::unordered_map<const void *, const XMLCh *> NameMap;

    XERCES_CPP_NAMESPACE_QUALIFIER MemoryManager *memMgr_;
    TypeMap                                       typePool_;
    NameMap                                       namePool_;
  public:
    /**
     * Constructs a new string pool for Protocol Buffer <-> XQilla string mapping.
     *
     * @param [in] memMgr the memory manager used for allocations.
     */
    XQStringPool(XERCES_CPP_NAMESPACE_QUALIFIER MemoryManager *memMgr = XERCES_CPP_NAMESPACE_QUALIFIER XMLPlatformUtils::fgMemoryManager);

    /// Desctructor. Call XQStringPool::clear().
    ~XQStringPool();

    /**
     * Retrieves the type URI and name associated with the specified message
     * descriptor.
     *
     * @param [in] desc the message descriptor.
     * @param [out] uri the message's type URI.
     * @param [out] name the message's type name
     */
    void getType(const Descriptor *desc, const XMLCh *&uri, const XMLCh *&name);

    /**
     * Retrieves the type URI and name associated with the specified field
     * descriptor.
     *
     * @param [in] desc the field descriptor.
     * @param [out] uri the field's type URI.
     * @param [out] name the field's type name.
     */
    void getType(const FieldDescriptor *desc, const XMLCh *&uri, const XMLCh *&name);

    /**
     * Retrieves the type URI and name associated with the specified enumeration
     * descriptor.
     *
     * @param [in] desc the enumeration descriptor.
     * @param [out] uri the enumeration's type URI.
     * @param [out] name the enumeration's type name.
     */
    void getType(const EnumDescriptor *desc, const XMLCh *&uri, const XMLCh *&name);

    /**
     * Returns the name of the specified field.
     *
     * @param [in] desc the field descriptor.
     * @return the field's name.
     */
    const XMLCh *getName(const FieldDescriptor *desc);

    /**
     * Returns the name of the specified enumeration value.
     *
     * @param [in] value the enumeration value descriptor.
     * @return the enumeration value's name.
     */
    const XMLCh *getName(const EnumValueDescriptor *value);

    void clear();
};

}

#endif /* !_PQ_XQSTRINGPOOL_HPP_ */
