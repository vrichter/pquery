/*
 * Copyright (c) 2012 Michael Beuse <mbeuse@techfak.uni-bielefeld.de>
 *
 * This file is part of PQuery.
 *
 * PQuery is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * PQuery is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with PQuery.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $Id$
 */
#ifndef _PQ_XQNODEIMPL_HPP_
#define _PQ_XQNODEIMPL_HPP_ 1

#include <pquery/XQNode.hpp>
#include <pquery/PQNode.hpp>

namespace pquery
{

class XQNodeImpl : public XQNode
{
  public:
    typedef RefCountPointer<const XQNodeImpl> Ptr;

    enum Type
    {
      DOCUMENT, ELEMENT, TEXT, TYPE_ATTRIBUTE
    };

  protected:
    io::PQNode::Ptr node_;
    const XMLCh    *localName_;
    const XMLCh    *typeURI_;
    const XMLCh    *typeName_;
    Type            type_;

  public:
    XQNodeImpl(const io::PQNode::Ptr &node, Type type = ELEMENT);

    virtual ~XQNodeImpl() {}

    /**
     * @name XQilla API implementation.
     */
    ///@{

    virtual const XMLCh *asString(const DynamicContext *context) const;
    virtual const XMLCh *getTypeURI() const;
    virtual const XMLCh *getTypeName() const;
    virtual void generateEvents(EventHandler *events, const DynamicContext *context,
                                bool preserveNS = true, bool preserveType = true) const;
    virtual void *getInterface(const XMLCh *name) const;
    virtual bool hasInstanceOfType(const XMLCh *typeURI, const XMLCh *typeName, const DynamicContext *context) const;
    virtual const XMLCh *dmNodeKind() const;
    virtual ATQNameOrDerived::Ptr dmNodeName(const DynamicContext *context) const;
    virtual const XMLCh *dmStringValue(const DynamicContext *context) const;
    virtual Sequence dmTypedValue(DynamicContext *context) const;
    virtual ATQNameOrDerived::Ptr dmTypeName(const DynamicContext *context) const;
    virtual ATBooleanOrDerived::Ptr dmNilled(const DynamicContext *context) const;
    virtual bool lessThan(const Node::Ptr &other, const DynamicContext *context) const;
    virtual bool equals(const Node::Ptr &other) const;
    virtual bool uniqueLessThan(const Node::Ptr &other, const DynamicContext *context) const;
    virtual Node::Ptr root(const DynamicContext *context) const;
    virtual Node::Ptr dmParent(const DynamicContext *context) const;
    virtual ::Result dmAttributes(const DynamicContext *context, const LocationInfo *info) const;
    virtual ::Result dmNamespaceNodes(const DynamicContext *context, const LocationInfo *info) const;
    virtual ::Result dmChildren(const DynamicContext *context, const LocationInfo *info) const;
    virtual ::Result getAxisResult(XQStep::Axis axis, const NodeTest *nodeTest,
                                 const DynamicContext *context, const LocationInfo *info) const;
    virtual ATBooleanOrDerived::Ptr dmIsId(const DynamicContext *context) const;
    virtual ATBooleanOrDerived::Ptr dmIsIdRefs(const DynamicContext *context) const;

    ///@}

    /**
     * @name PQuery XQNode implementation
     */
    ///@{

    virtual const PQ_PROTOBUF_NS Descriptor *getDescriptor() const;
    virtual const PQ_PROTOBUF_NS FieldDescriptor *getFieldDescriptor() const;
    virtual bool getValue(AtomicValue &value) const;
    virtual PQ_PROTOBUF_NS Message *getMessage() const;
    virtual bool serialize(PQ_PROTOBUF_NS io::ZeroCopyOutputStream *ostream) const;
    virtual bool serialize(::std::string &str) const;

    ///@}

    XQNodeImpl::Ptr firstChild() const;
    XQNodeImpl::Ptr nextSibling() const;
    XQNodeImpl::Ptr prevSibling() const;
    XQNodeImpl::Ptr parent() const;

    io::PQNode::Ptr getPQNode() const;
};

}

#endif /* !_PQ_XQNODEIMPL_HPP_ */
