/*
 * Copyright (c) 2012 Michael Beuse <mbeuse@techfak.uni-bielefeld.de>
 *
 * This file is part of PQuery.
 *
 * PQuery is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * PQuery is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with PQuery.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $Id$
 */
#ifndef _PQ_DOCUMENTCACHE_HPP_
#define _PQ_DOCUMENTCACHE_HPP_ 1

#include <pquery/common.hpp>
#include <xqilla/schema/DocumentCache.hpp>

namespace pquery
{

class XQDocumentCache : public DocumentCache
{
  protected:
    DocumentCache *wrapped_;
  public:
    XQDocumentCache(DocumentCache *wrapped) : wrapped_(wrapped) {}

    virtual ~XQDocumentCache() {}

    virtual Node::Ptr loadDocument(const XMLCh *uri, DynamicContext *context, const QueryPathNode *projections = 0)
    { return wrapped_->loadDocument(uri, context, projections); }

    virtual Node::Ptr parseDocument(XERCES_CPP_NAMESPACE_QUALIFIER InputSource &srcToUse, DynamicContext *context, const QueryPathNode *projection)
    { return wrapped_->parseDocument(srcToUse, context, projection); }

    virtual void parseDocument(XERCES_CPP_NAMESPACE_QUALIFIER InputSource &srcToUse, EventHandler *handler, DynamicContext *context)
    { wrapped_->parseDocument(srcToUse, handler, context); }

    virtual void setXMLEntityResolver(XERCES_CPP_NAMESPACE_QUALIFIER XMLEntityResolver * const handler)
    { wrapped_->setXMLEntityResolver(handler); }

    virtual XERCES_CPP_NAMESPACE_QUALIFIER XMLEntityResolver *getXMLEntityResolver() const
    { return wrapped_->getXMLEntityResolver(); }

    virtual bool getDoPSVI() const
    { return wrapped_->getDoPSVI(); }

    virtual void setDoPSVI(bool value)
    { wrapped_->setDoPSVI(value); }

    virtual bool isTypeOrDerivedFromType(const XMLCh * const uri, const XMLCh * const typeName,
                                         const XMLCh * const uriToCheck, const XMLCh * const typeNameToCheck) const
    { return wrapped_->isTypeOrDerivedFromType(uri, typeName, uriToCheck, typeNameToCheck); }

    virtual bool isTypeDefined(const XMLCh * const uri, const XMLCh * const typeName) const;

    virtual void addSchemaLocation(const XMLCh *uri, VectorOfStrings *locations, StaticContext *context, const LocationInfo *location)
    { wrapped_->addSchemaLocation(uri, locations, context, location); }

    virtual unsigned int getSchemaUriId(const XMLCh *uri) const
    { return wrapped_->getSchemaUriId(uri); }

    virtual const XMLCh *getSchemaUri(unsigned int id) const
    { return wrapped_->getSchemaUri(id); }

    virtual XERCES_CPP_NAMESPACE_QUALIFIER GrammarResolver *getGrammarResolver() const
    { return wrapped_->getGrammarResolver(); }

    virtual XERCES_CPP_NAMESPACE_QUALIFIER DatatypeValidator *getDatatypeValidator(const XMLCh *uri, const XMLCh *typeName) const
    { return wrapped_->getDatatypeValidator(uri, typeName); }

    virtual XERCES_CPP_NAMESPACE_QUALIFIER ComplexTypeInfo *getComplexTypeInfo(const XMLCh *uri, const XMLCh *typeName) const
    { return wrapped_->getComplexTypeInfo(uri, typeName); }

    virtual XERCES_CPP_NAMESPACE_QUALIFIER SchemaElementDecl *getElementDecl(const XMLCh *elementUri, const XMLCh *elementName) const
    { return wrapped_->getElementDecl(elementUri, elementName); }

    virtual XERCES_CPP_NAMESPACE_QUALIFIER SchemaAttDef *getAttributeDecl(const XMLCh *attributeUri, const XMLCh *attributeName) const
    { return wrapped_->getAttributeDecl(attributeUri, attributeName); }

    virtual DocumentCache *createDerivedCache(XERCES_CPP_NAMESPACE_QUALIFIER MemoryManager *memMgr) const
    { return wrapped_->createDerivedCache(memMgr); }

};

}

#endif /* !_PQ_DOCUMENTCACHE_HPP_ */
