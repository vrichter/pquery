/*
 * Copyright (c) 2012 Michael Beuse <mbeuse@techfak.uni-bielefeld.de>
 *
 * This file is part of PQuery.
 *
 * PQuery is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * PQuery is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with PQuery.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $Id$
 */
#ifndef _PQ_PQUERYIMPL_HPP_
#define _PQ_PQUERYIMPL_HPP_ 1

#include <pquery/pquery.hpp>
#include <pquery/PQNode.hpp>
#include <xqilla/runtime/Result.hpp>

class DynamicContext;
class XQQuery;

namespace pquery
{

class ItemImpl : public virtual Item
{
  public:
    typedef ::boost::shared_ptr<ItemImpl> Ptr;

    virtual ~ItemImpl() {}

    static const char itemImpl_string[];

    virtual io::PQNode::Ptr getNode() const = 0;
};

class MessageImpl : public ItemImpl, public Message
{
  protected:
    io::PQNode::Ptr node_;

  public:
    typedef ::boost::shared_ptr<MessageImpl> Ptr;

    MessageImpl(const io::PQNode::Ptr &node) : node_(node) {}

    virtual ~MessageImpl() {}

    virtual Item::Type getType() const { return MESSAGE; }
    virtual void *getInterface(const char *name) const;
    virtual std::string asString() const;
    virtual const std::string &getName() const;
    virtual const PQ_PROTOBUF_NS Descriptor *getDescriptor() const;
    virtual PQ_PROTOBUF_NS Message *getMessage() const;
    virtual bool serialize(std::string &str) const;

    virtual io::PQNode::Ptr getNode() const { return node_; }
};

class FieldImpl : public ItemImpl, public Field
{
  protected:
    io::PQNode::Ptr node_;

  public:
    typedef ::boost::shared_ptr<FieldImpl> Ptr;

    FieldImpl(const io::PQNode::Ptr &node) : node_(node) {}

    virtual ~FieldImpl() {}

    virtual Item::Type getType() const { return FIELD; }
    virtual void *getInterface(const char *name) const;
    virtual std::string asString() const;
    virtual const std::string &getName() const;
    virtual const FieldDescriptor *getDescriptor() const;
    virtual AtomicValue getValue() const;

    virtual io::PQNode::Ptr getNode() const { return node_; }
};

class AtomicImpl : public ItemImpl, public Atomic
{
  protected:
    AtomicValue value_;

  public:
    typedef ::boost::shared_ptr<AtomicImpl> Ptr;

    AtomicImpl(const AtomicValue &value) : value_(value) {}

    virtual ~AtomicImpl() {}

    virtual Item::Type getType() const { return ATOMIC; }
    virtual void *getInterface(const char *name) const;
    virtual std::string asString() const;
    virtual AtomicValue getValue() const;

    virtual io::PQNode::Ptr getNode() const { return io::PQNode::Ptr(); }
};

class ResultImplImpl : public ResultImpl
{
  protected:
    DynamicContext *ctx_;
    ::Result        result_;
  public:
    typedef ::boost::shared_ptr<ResultImplImpl> Ptr;

    ResultImplImpl(DynamicContext *ctx, ::Result result) :
      ctx_(ctx), result_(result) {}

    virtual ~ResultImplImpl() {}

    virtual Item::Ptr next();
};

class QueryImpl : public Query
{
  protected:
    DynamicContext *ctx_;
    XQQuery        *xqquery_;
    std::string     query_;

  public:
    QueryImpl(DynamicContext *context, XQQuery *query, const std::string &queryStr) :
      ctx_(context), xqquery_(query), query_(queryStr) {}

    virtual ~QueryImpl();

    virtual const std::string &getQuery() const { return query_; }
    virtual Result execute(const Item::Ptr &context);
};

}

#endif /* !_PQ_PQUERYIMPL_HPP_ */
