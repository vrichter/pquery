/*
 * Copyright (c) 2012 Michael Beuse <mbeuse@techfak.uni-bielefeld.de>
 *
 * This file is part of PQuery.
 *
 * PQuery is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * PQuery is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with PQuery.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $Id$
 */
/**
 * @file common.hpp Provides common data type and includes used throughout the project.
 */
#ifndef _PQ_COMMON_HPP_
#define _PQ_COMMON_HPP_ 1

#include <cstdlib>
#include <string>
#include <exception>

#include <boost/shared_ptr.hpp>
#include <google/protobuf/stubs/common.h>

#define PQ_PROTOBUF_NS ::google::protobuf::

namespace pquery
{

///@{
/**
 * Fixed-width integer types. Included from Protocol Buffers headers for API
 * consistency and interoperability.
 */

using ::google::protobuf::int8;
using ::google::protobuf::int16;
using ::google::protobuf::int32;
using ::google::protobuf::int64;
using ::google::protobuf::uint8;
using ::google::protobuf::uint16;
using ::google::protobuf::uint32;
using ::google::protobuf::uint64;

///@}


class PQException : public ::std::exception
{
  protected:
    const char    *function_;
    const char    *reason_;
    const char    *file_;
    unsigned int   line_;
    ::std::string  what_;

  public:
    PQException(const char *fn = 0, const char *reason = 0, const char *file = 0, unsigned int line = 0) :
      function_(fn), reason_(reason), file_(file), line_(line), what_("PQuery Exception:")
    {
      if (0 != function_)
      {
        what_ += function_;
        what_ += ":";
      }
      if (0 != file_)
      {
        what_ += file_;
        what_ += ":";
        what_ += line_;
        what_ += ":";
      }
      if (0 != reason)
      {
        what_ += " ";
        what_ += reason;
      }
    }

    virtual ~PQException() throw() {}

    const char *getFunction() const { return function_; }
    const char *getReason() const { return reason_; }
    const char *getFile() const { return file_; }
    unsigned int getLine() const { return line_; }

    virtual const char *what() const throw() { return what_.c_str(); }
};

#define PQTHROW(fn, reason) PQException(fn, reason, __FILE__, __LINE__)

}

#endif /* !_PQ_COMMON_HPP_ */
