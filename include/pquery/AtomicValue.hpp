/*
 * Copyright (c) 2012 Michael Beuse <mbeuse@techfak.uni-bielefeld.de>
 *
 * This file is part of PQuery.
 *
 * PQuery is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * PQuery is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with PQuery.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $Id$
 */
#ifndef _PQ_ATOMICVALUE_HPP_
#define _PQ_ATOMICVALUE_HPP_ 1

#include <pquery/common.hpp>
#include <sstream>
#include <google/protobuf/descriptor.h>

namespace pquery
{

using ::google::protobuf::FieldDescriptor;
using ::google::protobuf::EnumValueDescriptor;
using ::std::stringstream;

/**
 * Offers the capability of handling the value types supported Protocol Buffers
 * in a convenient way.
 */
class AtomicValue
{
  friend ::std::ostream &operator<<(::std::ostream &, const AtomicValue &);

  public:
    enum CppType
    {
      BOOL, INT32, UINT32, INT64, UINT64, FLOAT, DOUBLE, STRING
    };

  protected:
    FieldDescriptor::Type type_;

  public:
    union {
      bool   bool_value;
      int32  int32_value;
      uint32 uint32_value;
      int64  int64_value;
      uint64 uint64_value;
      float  float_value;
      double double_value;
      const EnumValueDescriptor *enum_value;
    } atomic_value_;

  protected:
    stringstream string_value_;


  public:
    /**
     *
     */
    AtomicValue();

    ///@{
    /**
     * Value constructors.
     *
     * @note All constructors are implicit by design so working with AtomicValue
     *       should be as convenient as possible by constructing instances where
     *       they are required.
     */
    AtomicValue(bool value, FieldDescriptor::Type type = FieldDescriptor::TYPE_BOOL);
    AtomicValue(int32 value, FieldDescriptor::Type type = FieldDescriptor::TYPE_INT32);
    AtomicValue(uint32 vlaue, FieldDescriptor::Type type = FieldDescriptor::TYPE_UINT32);
    AtomicValue(int64 value, FieldDescriptor::Type type = FieldDescriptor::TYPE_INT64);
    AtomicValue(uint64 value, FieldDescriptor::Type type = FieldDescriptor::TYPE_UINT64);
    AtomicValue(float value, FieldDescriptor::Type type = FieldDescriptor::TYPE_FLOAT);
    AtomicValue(double value, FieldDescriptor::Type type = FieldDescriptor::TYPE_DOUBLE);
    AtomicValue(const std::string &value, FieldDescriptor::Type type = FieldDescriptor::TYPE_STRING);
    AtomicValue(const uint8 *bytes, size_t len, FieldDescriptor::Type type = FieldDescriptor::TYPE_BYTES);
    AtomicValue(const EnumValueDescriptor *value, FieldDescriptor::Type type = FieldDescriptor::TYPE_ENUM);
    ///@}

    /// Copy constructor.
    AtomicValue(const AtomicValue &other);

    /// Assignment operator
    AtomicValue &operator=(const AtomicValue &other);

    /// Returns the Protocol Buffer type of the value.
    FieldDescriptor::Type getType() const { return type_; }

    /// Returns a string representation of this value.
    std::string asString() const;

    CppType getCppType() const;

    operator bool() const;

  protected:
    /// Updates the string value according to specified type.
    void updateStringValue();
};

}

#endif /* !_PQ_ATOMICVALUE_HPP_ */
