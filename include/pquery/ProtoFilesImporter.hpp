/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : include/pquery/ProtoFilesImporter.h                    **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <google/protobuf/compiler/importer.h>
#include <boost/shared_ptr.hpp>

namespace pquery {

  class PrintingErrorCollector : public google::protobuf::compiler::MultiFileErrorCollector {
    void AddError(const std::string & filename, int line, int column, const std::string & message);
  };

  class ProtoFilesImporter {
  public:

    typedef boost::shared_ptr<ProtoFilesImporter> Ptr;

    ProtoFilesImporter(const std::string& path, const std::string delimiters=":");

    const google::protobuf::Descriptor* findDescriptor(const std::string& typeName);

    static bool isBasicType(const std::string& type);

  private:
    google::protobuf::compiler::DiskSourceTree m_SourceTree;
    PrintingErrorCollector m_ErrorCollector;
    google::protobuf::compiler::Importer m_Importer;
    std::set<std::string> m_Imported;
  };

} // namespace pquery
