/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : include/pquery/AnyTypeParticipantConfig.h              **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#pragma once

#include <rsb/ParticipantConfig.h>

namespace pquery {

  class AnyTypeParticipantConfig : public rsb::ParticipantConfig{
  public:
    AnyTypeParticipantConfig();
    AnyTypeParticipantConfig(const rsb::ParticipantConfig& config);

  private:
    void init();

  };

} // namespace pquery
