/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : app/example.cpp                                        **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <boost/program_options.hpp>
#include <boost/lockfree/spsc_queue.hpp>

#include <xqilla/exceptions/XQException.hpp>

#include <pquery/AnyTypeParticipantConfig.hpp>
#include <pquery/ProtoFilesImporter.hpp>
#include <pquery/pquery.hpp>
#include <pquery/pqueryImpl.hpp>
#include <pquery/PQNode.hpp>

#include <rsb/Factory.h>
#include <rsb/Scope.h>

#include <signal.h>

#include <google/protobuf/dynamic_message.h>

typedef boost::shared_ptr<rsb::Event> EventPtr;
typedef boost::lockfree::spsc_queue<EventPtr> EventPtrQueue;

// loads message definitions from proto files
pquery::ProtoFilesImporter::Ptr protoFiles;
// instance of pquery used to create queryable messages from rsb wiredata
pquery::PQuery pqueryInst;
// the query generated from an xpath
pquery::Query::Ptr query;

void printItem(const std::string& prefix, pquery::Item::Ptr item, std::ostream& stream){
  switch (item->getType()){
    case pquery::Item::ATOMIC:
      {
        pquery::Atomic::Ptr a = boost::dynamic_pointer_cast<pquery::Atomic>(item);
        stream << prefix << a->asString() << "\n";
      }
      break;
    case pquery::Item::FIELD:
      {
        pquery::Field::Ptr f = boost::dynamic_pointer_cast<pquery::Field>(item);
        stream << prefix << f->getName() << ": " << f->getValue().asString() << "\n";
      }
      break;
    case pquery::Item::MESSAGE:
      {
        pquery::MessageImpl::Ptr m = boost::dynamic_pointer_cast<pquery::MessageImpl>(item);
        stream << prefix << m->getNode()->DebugString() << "\n";
      }
      break;
    default:
      assert(false);
  }
}

EventPtrQueue events(100);
void addToList(boost::shared_ptr<rsb::Event> event) {
  events.push(event);
}

void doQuery(EventPtr event) {
  std::string status = "in function header. This should not happen.";
  try {
    status = "casting input data";
    boost::shared_ptr<rsb::AnnotatedData> data = boost::static_pointer_cast<rsb::AnnotatedData>(event->getData());
    boost::shared_ptr<std::string> realData = boost::static_pointer_cast<std::string>(data->second);

    status = "searching message descriptor";
    // get message descriptor. this is mandatory because thw wire data is only binary
    const google::protobuf::Descriptor* descriptor = protoFiles->findDescriptor(data->first);
    if(descriptor == NULL){
      std::cout << "could not find descriptor for type: " << data->first << "." << std::endl;
      return;
    }
    status = "creating queryable message object";
    pquery::Item::Ptr message = pqueryInst.getMessage(descriptor, realData.get());
    if(realData.get()->empty()){
      std::cout << "### Empty message of type: " << data->first << std::endl; //TODO fix queries on empty messages
    } else {
      std::cout << "### Query on message type: " << data->first << std::endl;
    }
    status = "querying message";
    pquery::Result result = query->execute(message);

    //// print the results
    status = "iterating query results";
    int count = 0;
    while (pquery::Item::Ptr item = result.next()){
      ++count;
      std::cout << "##  Match no: " << count << "\n";
      printItem("    ", item, std::cout);
    }
    std::cout << "## Matches: " << count << std::endl;
  } catch (const std::exception &e){
    std::cerr << "Catched error while " << status << ": Error" << e.what() << std::endl;
  } catch (const  XQException &e){
    std::cerr << "Catched xquila exception while " << status << ": Error debug printed." << std::endl;
    e.printDebug(e.getError());
  }
}

int main(int n, char**ppc){
  // map for program options
  boost::program_options::variables_map vm;
  boost::program_options::options_description desc("Allowed options");
  desc.add_options()
      ("help,h","produce help message")

      ("scope,s",
       boost::program_options::value<std::string>()->default_value("/"),
       "The scope to listen to.")

      ("xpath,x",
       boost::program_options::value<std::string>()->default_value("/message"),
       "The xpath to evaluate on the message. The root of a protobuf xpath is /message")

      ("proto,p",
       boost::program_options::value<std::string>()->default_value(RST_PROTO_PATH),
       "A path to proto files root folder. For example: " RST_PROTO_PATH)
      ;

  boost::program_options::store(boost::program_options::parse_command_line(n, ppc, desc), vm);
  boost::program_options::notify(vm);

  if (vm.count("help")) {
    std::cout << desc << "\n";
    return 1;
  }

  std::cerr << "Starting pquery.\n"
            << "-Listen to scope: " << vm["scope"].as<std::string>() << "\n"
            << "-XPath " << vm["xpath"].as<std::string>() << "\n"
            << "-Proto files from: " << vm["proto"].as<std::string>() << "\n"
            << std::endl;

  // create importer for message definitions
  protoFiles = pquery::ProtoFilesImporter::Ptr(new pquery::ProtoFilesImporter(vm["proto"].as<std::string>()));

  // create query
  try {
    query = pqueryInst.parse(vm["xpath"].as<std::string>());
  } catch (const std::exception& e) {
    std::cerr << "Cant parse query: \n"
              << "xpath: '" << vm["xpath"].as<std::string>() << "'\n"
              << "error: " << e.what() << std::endl;
    exit(-1);
  }

  // get the factory
  rsb::Factory& factory = rsb::getFactory();

  // create listener
  rsb::ListenerPtr listener = factory.createListener(
                                rsb::Scope(vm["scope"].as<std::string>()),pquery::AnyTypeParticipantConfig());

  // register the query handler
  //listener->addHandler(rsb::HandlerPtr(new rsb::EventFunctionHandler(&doQuery)));
  listener->addHandler(rsb::HandlerPtr(new rsb::EventFunctionHandler(&addToList)));

  std::cerr << "Ready...";
  std::cerr << "\nEnter 'q' or 'quit' to close application.\n" << std::endl;

  //std::string in = "";
  //while(in != "q" && in != "quit"){
  //  std::cin >> in;
  //}

  while(true) {
    events.consume_one(&doQuery);
  }

  return 0;
}
