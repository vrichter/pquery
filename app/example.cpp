/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : app/example.cpp                                        **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <boost/program_options.hpp>

#include <pquery/AnyTypeParticipantConfig.hpp>
#include <pquery/ProtoFilesImporter.hpp>
#include <pquery/pquery.hpp>

#include <rsb/Factory.h>
#include <rsb/Scope.h>

#include <signal.h>

// loads message definitions from proto files
pquery::ProtoFilesImporter::Ptr protoFiles;
// instance of pquery used to create queryable messages from rsb wiredata
pquery::PQuery pqueryInst;
// the query generated from an xpath
pquery::Query::Ptr query;

void printItem(pquery::Item::Ptr item){
  switch (item->getType()){
    case pquery::Item::ATOMIC:
      {
        pquery::Atomic::Ptr a = boost::dynamic_pointer_cast<pquery::Atomic>(item);
        std::cout << "atomic value: " << a->asString() << std::endl;
      }
      break;
    case pquery::Item::FIELD:
      {
        pquery::Field::Ptr f = boost::dynamic_pointer_cast<pquery::Field>(item);
        std::cout << "field value: " << f->getValue().asString() << std::endl;
      }
      break;
    case pquery::Item::MESSAGE:
      {
        pquery::Message::Ptr m = boost::dynamic_pointer_cast<pquery::Message>(item);
        std::cout << "message string: " << m->asString() << std::endl;
        std::cout << "message name: " << m->getName() << std::endl;
        std::cout << "message type: " << m->getType() << std::endl;
      }
      break;
    default:
      assert(false);
  }
}

void doQuery(boost::shared_ptr<rsb::AnnotatedData> data) {
  boost::shared_ptr<std::pair<std::string, boost::shared_ptr<std::string> > > realData =
      boost::static_pointer_cast<std::pair<std::string, boost::shared_ptr<std::string> > >(
        data->second);

  // get message descriptor. this is mandatory because thw wire data is only binary
  const google::protobuf::Descriptor* descriptor = protoFiles->findDescriptor(data->first);
  if(descriptor == NULL){
    std::cout << "could not find descriptor for type: " << data->first << "." << std::endl;
    return;
  }

  // create message object
  pquery::Item::Ptr message = pqueryInst.getMessage(descriptor, &realData->first);

  // query the message
  std::cout << "Querying on message type: " << data->first << std::endl;
  pquery::Result result = query->execute(message);

  // print the results
  int count = 0;
  while (pquery::Item::Ptr item = result.next()){
    printItem(item);
    ++count;
  }
  std::cout << "Got results: " << count << std::endl;
}

int main(int n, char**ppc){
  // map for program options
  boost::program_options::variables_map vm;
  boost::program_options::options_description desc("Allowed options");
  desc.add_options()
      ("help","produce help message")

      ("scope",
       boost::program_options::value<std::string>()->default_value("/"),
       "The scope to listen to.")

      ("xpath",
       boost::program_options::value<std::string>()->default_value("/message/width"),
       "The xpath to evaluate on the message. The root of a protobuf xpath is /message")

      ("proto",
       boost::program_options::value<std::string>()->default_value(RST_PROTO_PATH),
       "A path to proto files root folder. For example: " RST_PROTO_PATH)
      ;

  boost::program_options::store(boost::program_options::parse_command_line(n, ppc, desc), vm);
  boost::program_options::notify(vm);

  if (vm.count("help")) {
    std::cout << desc << "\n";
    return 1;
  }

  std::cout << "Starting pquery-example.\n"
            << "-Listen to scope: " << vm["scope"].as<std::string>() << "\n"
            << "-XPath " << vm["xpath"].as<std::string>() << "\n"
            << "-Proto files from: " << vm["proto"].as<std::string>() << "\n"
            << std::endl;

  // create importer for message definitions
  protoFiles = pquery::ProtoFilesImporter::Ptr(new pquery::ProtoFilesImporter(vm["proto"].as<std::string>()));

  // create query
  try {
    query = pqueryInst.parse(vm["xpath"].as<std::string>());
  } catch (const std::exception& e) {
    std::cout << "Cant parse query: \n"
              << "xpath: '" << vm["xpath"].as<std::string>() << "'\n"
              << "error: " << e.what() << std::endl;
    exit(-1);
  }

  // get the factory
  rsb::Factory& factory = rsb::getFactory();

  // create listener
  rsb::ListenerPtr listener = factory.createListener(
                                rsb::Scope(vm["scope"].as<std::string>()),pquery::AnyTypeParticipantConfig());

  // register the query handler
  listener->addHandler(rsb::HandlerPtr(new rsb::DataFunctionHandler<rsb::AnnotatedData> (&doQuery)));

  std::cout << "Ready...";

  std::string in = "";
  sleep(50);
  while(in != "q" && in != "quit"){
    std::cout << "\nEnter 'q' or 'quit' to close application\n> " << std::flush;
    std::cin >> in;
  }

  return 0;
}
