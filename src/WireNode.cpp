/*
 * Copyright (c) 2012 Michael Beuse <mbeuse@techfak.uni-bielefeld.de>
 *
 * This file is part of PQuery.
 *
 * PQuery is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * PQuery is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with PQuery.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $Id$
 */

#include <iostream>
#include <pquery/WireNode.hpp>

#include <utility>

#include <google/protobuf/descriptor.h>

#include <boost/assert.hpp>
#include <boost/make_shared.hpp>
#include <boost/enable_shared_from_this.hpp>

namespace pquery
{
namespace io
{

  static const std::string INDENT = "    ";

using namespace ::google::protobuf;

// ----------------------------------------------------------------------------
// WireNode class
// ----------------------------------------------------------------------------

struct WireBaseData;

class WireNode : public PQNode,
                 public ::boost::enable_shared_from_this<WireNode>
{
  public:
    typedef ::boost::shared_ptr<const WireNode> Ptr;

    static const char wirenode_string[];

  protected:
    WireNode::Ptr parent_;
    WireNode::Ptr prev_;
    WireBaseData *data_;

  public:
    WireNode(WireBaseData *data) : data_(data) {}
    WireNode(const WireNode::Ptr &parent, WireBaseData *data) : parent_(parent), data_(data) {}
    WireNode(const WireNode::Ptr &parent, const WireNode::Ptr &prev, WireBaseData *data) :
      parent_(parent), data_(data) {}

    virtual ~WireNode();

    virtual void *getInterface(const char *name) const;
    virtual NodeType getType() const;
    virtual PQNode::Ptr root() const;
    virtual PQNode::Ptr parent() const;
    virtual PQNode::Ptr firstChild() const;
    virtual PQNode::Ptr nextSibling() const;
    virtual PQNode::Ptr previousSibling() const;

    virtual bool getValue(AtomicValue &value) const;

    virtual const Descriptor *getDescriptor() const;
    virtual const FieldDescriptor *getFieldDescriptor() const;

    virtual bool equals(const PQNode::Ptr &other) const;

    virtual std::string DebugString() const;
};

const char
WireNode::wirenode_string[] = "WireNode";

// ----------------------------------------------------------------------------
// Processing functions
// ----------------------------------------------------------------------------

typedef ::std::string::iterator Pos;
typedef ::std::pair<Pos, Pos> Range;

struct WireBaseData
{
  enum Type
  {
    ROOT, MESSAGE_FIELD, PACKED_FIELD, ATOMIC_FIELD, LEN_DELIM_FIELD, EOS_MARKER
  };

  Type type;

  WireBaseData(Type t) : type(t) {}
};

struct WireEOSMarker : public WireBaseData
{
  WireEOSMarker() : WireBaseData(WireBaseData::EOS_MARKER) {}
};

struct WireRootData : public WireBaseData
{
  const Descriptor *message;

  ::std::string *string;
  Range content;

  WireRootData(const Descriptor *m, ::std::string *s) :
    WireBaseData(WireBaseData::ROOT), message(m), string(s), content(string->begin(), string->end()) {}
};

struct WireFieldBaseData : public WireBaseData
{
  const FieldDescriptor *field;

  Range tag;
  Range content;

  WireFieldBaseData(WireBaseData::Type t, const FieldDescriptor *f, const Range &tr, const Range &cr) :
    WireBaseData(t), field(f), tag(tr), content(cr) {}
};

struct WireLenDelimBaseData : public WireFieldBaseData
{
  Range size;

  WireLenDelimBaseData(WireBaseData::Type t, const FieldDescriptor *f, const Range &tr, const Range &cr, const Range &sr) :
    WireFieldBaseData(t, f, tr, cr), size(sr) {}
};

struct WirePackedData : public WireLenDelimBaseData
{
  Range packed;

  WirePackedData(const FieldDescriptor *f, const Range &tr, const Range &cr, const Range &sr, const Range &pr) :
    WireLenDelimBaseData(WireBaseData::PACKED_FIELD, f, tr, cr, sr), packed(pr) {}
};

struct WireMessageData : public WireLenDelimBaseData
{
  const Descriptor *message;

  WireMessageData(const FieldDescriptor *f, const Range &tr, const Range &cr, const Range &sr, const Descriptor *m) :
    WireLenDelimBaseData(WireBaseData::MESSAGE_FIELD, f, tr, cr, sr), message(m) {}
};

struct WireAtomicData : public WireFieldBaseData
{
  WireAtomicData(const FieldDescriptor *f, const Range &tr, const Range &cr) :
    WireFieldBaseData(WireBaseData::ATOMIC_FIELD, f, tr, cr) {}
};

struct WireLenDelimData : public WireLenDelimBaseData
{
  WireLenDelimData(const FieldDescriptor *f, const Range &tr, const Range &cr, const Range &sr) :
    WireLenDelimBaseData(WireBaseData::LEN_DELIM_FIELD, f, tr, cr, sr) {}
};

/**
 * @todo add overflow check
 * @param in
 * @param from
 * @return
 */
static inline uint64
readVarInt(const Range &in, Range &from)
{
  BOOST_ASSERT(in.first <= from.first);

  from.second = from.first;

  uint64 value(0);
  int    shift(0);

  do
  {
    BOOST_ASSERT(from.second < in.second);

    value |= static_cast<uint64>(*from.second & 0x7F) << shift;
    shift += 7;
  } while (0 != (*from.second++ & 0x80));

  return value;
}

static inline void
lenVarInt(const Range &in, Range &from)
{
  BOOST_ASSERT(in.first <= from.first);

  from.second = from.first;

  while (0 != (*from.second++ & 0x80))
    BOOST_ASSERT(from.second <= in.second);
}

/**
 *
 * @param in
 * @param from
 * @return
 */
static inline uint32
readFixed32(const Range &in, Range &from)
{
  BOOST_ASSERT(in.first <= from.first);
  BOOST_ASSERT(from.first + 4 <= in.second);

  from.second = from.first;

  uint32 value(static_cast<uint32>(*from.second++));

  value |= static_cast<uint32>(*from.second++) <<  8;
  value |= static_cast<uint32>(*from.second++) << 16;
  value |= static_cast<uint32>(*from.second++) << 24;

  return value;
}

/**
 *
 * @param in
 * @param from
 */
static inline void
lenFixed32(const Range &in, Range &from)
{
  BOOST_ASSERT(in.first <= from.first);
  BOOST_ASSERT(from.first + 4 <= in.second);

  from.second = from.first + 4;
}

/**
 *
 * @param in
 * @param from
 * @return
 */
static inline uint64
readFixed64(const Range &in, Range &from)
{
  BOOST_ASSERT(in.first <= from.first);
  BOOST_ASSERT(from.first + 8 <= in.second);

  from.second = from.first;

  uint64 value(static_cast<uint64>(*from.second++));

  value |= static_cast<uint64>(*from.second++) <<  8;
  value |= static_cast<uint64>(*from.second++) << 16;
  value |= static_cast<uint64>(*from.second++) << 24;
  value |= static_cast<uint64>(*from.second++) << 32;
  value |= static_cast<uint64>(*from.second++) << 40;
  value |= static_cast<uint64>(*from.second++) << 48;
  value |= static_cast<uint64>(*from.second++) << 56;

  return value;
}

static inline void
lenFixed64(const Range &in, Range &from)
{
  BOOST_ASSERT(in.first <= from.first);
  BOOST_ASSERT(from.first + 8 <= in.second);

  from.second = from.first + 8;
}

static inline ::std::string
readString(const Range &in, Range &from)
{
  BOOST_ASSERT(in.first <= from.first);

  BOOST_ASSERT(from.second <= in.second);

  return ::std::string(from.first, from.second);
}

static inline void
lenString(const Range &in, Range &from, Range &size)
{
  BOOST_ASSERT(in.first <= from.first);

  // read size
  size.first = from.first;
  uint64 len(readVarInt(in, size));

  // adjust from range
  from.first  = size.second;
  from.second = from.first + len;

  BOOST_ASSERT(from.second <= in.second);
}

static inline float
readFloat(const Range &in, Range &from)
{
  BOOST_ASSERT(in.first <= from.first); BOOST_ASSERT(from.first + 4 <= in.second);

  from.second = from.first;

  float value;
  *(reinterpret_cast<unsigned char *>(&value)+0) = *from.second++;
  *(reinterpret_cast<unsigned char *>(&value)+1) = *from.second++;
  *(reinterpret_cast<unsigned char *>(&value)+2) = *from.second++;
  *(reinterpret_cast<unsigned char *>(&value)+3) = *from.second++;

  return value;
}

static inline double
readDouble(const Range &in, Range &from)
{
  BOOST_ASSERT(in.first <= from.first); BOOST_ASSERT(from.first + 8 <= in.second);

  from.second = from.first;

  double value;
  *(reinterpret_cast<unsigned char *>(&value)+0) = *from.second++;
  *(reinterpret_cast<unsigned char *>(&value)+1) = *from.second++;
  *(reinterpret_cast<unsigned char *>(&value)+2) = *from.second++;
  *(reinterpret_cast<unsigned char *>(&value)+3) = *from.second++;
  *(reinterpret_cast<unsigned char *>(&value)+4) = *from.second++;
  *(reinterpret_cast<unsigned char *>(&value)+5) = *from.second++;
  *(reinterpret_cast<unsigned char *>(&value)+6) = *from.second++;
  *(reinterpret_cast<unsigned char *>(&value)+7) = *from.second++;

  return value;
}

/**
 *
 * @param value
 * @return
 */
static inline int64
zigzag(uint64 value)
{
  // found at stackoverflow.com: thanks 3lectrologos
  return static_cast<int64>((value >> 1) ^ (-(value & 1)));
}

enum WireType
{
  VARINT      = 0,
  FIXED64     = 1,
  LEN_DELIM   = 2,
  GROUP_START = 3,
  GROUP_END   = 4,
  FIXED32     = 5
};

static void
printData(const char *fn, const WireBaseData *data, std::ostream& stream)
{
#if 0
  if (0 == data)
  {
    stream << fn << ": NULL data" << "\n";
    return;
  }
  switch (data->type)
  {
    case WireBaseData::ATOMIC_FIELD:
      {
        const WireAtomicData *atom(static_cast<const WireAtomicData *>(data));
        stream << fn << ": ATOMIC" << "\n";
        Range tag(atom->tag);
        stream << "  Tag Size: " << std::distance(atom->tag.first, atom->tag.second);
        stream << "  Tag: " << readVarInt(atom->tag, tag) << "\n";
        stream << "  Content: " << std::distance(atom->content.first, atom->content.second) << "\n";
        if (0 == atom->field)
          stream << "  Name: UNKNOWN" << "\n";
        else
          stream << "  Name: " << atom->field->name() << "\n";
        BOOST_ASSERT(atom->tag.second == atom->content.first);
      }
      break;
    case WireBaseData::LEN_DELIM_FIELD:
      {
        const WireLenDelimData *lenDelim(static_cast<const WireLenDelimData *>(data));
        stream << fn << ": LEN_DELIN" << "\n";
        Range tag(lenDelim->tag);
        stream << "  Tag Size: " << std::distance(lenDelim->tag.first, lenDelim->tag.second);
        stream << "  Tag: " << readVarInt(lenDelim->tag, tag) << "\n";
        stream << "  Size size: " << std::distance(lenDelim->size.first, lenDelim->size.second);
        tag = lenDelim->size;
        stream << "  Size: " << readVarInt(lenDelim->size, tag) << "\n";
        stream << "  Content: " << std::distance(lenDelim->content.first, lenDelim->content.second) << "\n";;
        if (0 == lenDelim->field)
          stream << "  Name: UNKNOWN" << "\n";
        else
          stream << "  Name: " << lenDelim->field->name() << "\n";
        BOOST_ASSERT(lenDelim->tag.second == lenDelim->size.first);
        BOOST_ASSERT(lenDelim->size.second == lenDelim->content.first);
      }
      break;
    case WireBaseData::MESSAGE_FIELD:
      {
        const WireMessageData *msg(static_cast<const WireMessageData *>(data));
        stream << fn << ": MESSAGE" << "\n";
        Range tag(msg->tag);
        stream << "  Tag Size: " << std::distance(msg->tag.first, msg->tag.second);
        stream << "  Tag: " << readVarInt(msg->tag, tag) << "\n";
        tag = msg->size;
        stream << "  Size size: " << std::distance(msg->size.first, msg->size.second);
        stream << "  Size: " << readVarInt(msg->size, tag) << "\n";
        stream << "  Content: " << std::distance(msg->content.first, msg->content.second) << "\n";;
        if (0 == msg->field)
          stream << "  Name: UNKNOWN" << "\n";
        else
          stream << "  Name: " << msg->field->name() << "\n";
        stream << "  Type: " << msg->message->full_name() << "\n";
        BOOST_ASSERT(msg->tag.second == msg->size.first);
        BOOST_ASSERT(msg->size.second == msg->content.first);
      }
      break;
    case WireBaseData::PACKED_FIELD:
      {
        const WirePackedData *packed(static_cast<const WirePackedData *>(data));
        stream << fn << ": PACKED" << "\n";
        Range tag(packed->tag);
        stream << "  Tag Size: " << std::distance(packed->tag.first, packed->tag.second);
        stream << "  Tag: " << readVarInt(packed->tag, tag) << "\n";
        tag = packed->size;
        stream << "  Size size: " << std::distance(packed->size.first, packed->size.second);
        stream << "  Size: " << readVarInt(packed->size, tag) << "\n";
        stream << "  Packed: " << std::distance(packed->packed.first, packed->packed.second) << "\n";
        stream << "  Content: " << std::distance(packed->content.first, packed->content.second) << "\n";;
        if (0 == packed->field)
          stream << "  Name: UNKNOWN" << "\n";
        else
          stream << "  Name: " << packed->field->name() << "\n";
        BOOST_ASSERT(packed->tag.second == packed->size.first);
        BOOST_ASSERT(packed->size.second == packed->content.first);
      }
      break;
    case WireBaseData::ROOT:
      {
        const WireRootData *root(static_cast<const WireRootData *>(data));
        stream << fn << ": ROOT" << "\n";
        stream << "  String: " << root->string << "\n";
        stream << "  Content: " << std::distance(root->content.first, root->content.second) << "\n";;
        stream << "  Type: " << root->message->full_name() << "\n";
      }
      break;
    default:
      BOOST_ASSERT(false);
  }
#endif
}

static WireType
typeToWireType(FieldDescriptor::Type type)
{
  switch (type)
  {
    case FieldDescriptor::TYPE_MESSAGE:
    case FieldDescriptor::TYPE_STRING:
    case FieldDescriptor::TYPE_BYTES:
      return LEN_DELIM;
    case FieldDescriptor::TYPE_BOOL:
    case FieldDescriptor::TYPE_UINT32:
    case FieldDescriptor::TYPE_UINT64:
    case FieldDescriptor::TYPE_INT32:
    case FieldDescriptor::TYPE_INT64:
    case FieldDescriptor::TYPE_SINT32:
    case FieldDescriptor::TYPE_SINT64:
      return VARINT;
    case FieldDescriptor::TYPE_FIXED32:
    case FieldDescriptor::TYPE_FLOAT:
    case FieldDescriptor::TYPE_SFIXED32:
      return FIXED32;
    case FieldDescriptor::TYPE_FIXED64:
    case FieldDescriptor::TYPE_DOUBLE:
    case FieldDescriptor::TYPE_SFIXED64:
      return FIXED64;
    default:
      throw PQTHROW("typeToWireType", "Unknown field type!");
  }
}

static bool
lenContent(uint64 tag, const Range &in, Range &content, Range &size)
{
  bool lenDelim(false);

  switch (tag & 0x07)
  {
    case VARINT:
      lenVarInt(in, content);
      break;
    case FIXED64:
      lenFixed64(in, content);
      break;
    case LEN_DELIM:
      lenString(in, content, size);
      lenDelim = true;
      break;
    case FIXED32:
      lenFixed32(in, content);
      break;
    case GROUP_START:
    case GROUP_END:
      PQTHROW("lenContent()", "Group are deprecated and not supported!");
      break;
    default:
      PQTHROW("lenContent()", "invalid encoded data!");
  }
  return lenDelim;
}

/**
 *
 * @param descriptor
 * @param in
 * @return
 */
static WireBaseData *
readFirstFromMessage(const Descriptor *descriptor, const Range &in)
{
  BOOST_ASSERT(0 != descriptor); BOOST_ASSERT(in.first < in.second);

  WireBaseData *child(0);

  // read tag
  Range tag_range(in);
  uint64 tag(readVarInt(in, tag_range));

  // determine content length
  Range size_range(tag_range.second, tag_range.second);
  Range content_range(tag_range.second, tag_range.second);
  bool isLenDelim(lenContent(tag, in, content_range, size_range));

  // check if field is known
  const FieldDescriptor *field(descriptor->FindFieldByNumber(static_cast<int>(tag >> 3)));

  if (0 != field)
  {
    if (FieldDescriptor::TYPE_MESSAGE == field->type())
      child = new WireMessageData(field, tag_range, content_range, size_range, field->message_type());
    else if (isLenDelim && field->is_packable())
    {
      Range packed_range(content_range);

      switch (typeToWireType(field->type()))
      {
        case VARINT:
          lenVarInt(content_range, packed_range);
          break;
        case FIXED32:
          lenFixed32(content_range, packed_range);
          break;
        case FIXED64:
          lenFixed64(content_range, packed_range);
          break;
        default:
          // should never happen
          BOOST_ASSERT(false);
          break;
      }
      child = new WirePackedData(field, tag_range, content_range, size_range, packed_range);
    }
    else if (isLenDelim)
    {
      child = new WireLenDelimData(field, tag_range, content_range, size_range);
    }
    else
    {
      child = new WireAtomicData(field, tag_range, content_range);
    }
  }
  else
  {
    if (isLenDelim)
      child = new WireLenDelimData(0, tag_range, content_range, size_range);
    else
      child = new WireAtomicData(0, tag_range, content_range);
  }

  return child;
}

static WireBaseData *
readNextFromMessage(const WireBaseData *parent, const Pos &from)
{
  BOOST_ASSERT(0 != parent);

  Range in;
  const Descriptor *desc(0);

  if (WireBaseData::ROOT == parent->type)
  {
    const WireRootData *root(static_cast<const WireRootData *>(parent));
    in   = root->content;
    desc = root->message;
  }
  else
  {
    const WireMessageData *msg(static_cast<const WireMessageData *>(parent));
    in   = msg->content;
    desc = msg->message;
  }

  // check if at the end
  if (from >= in.second)
    return 0;

  in.first = from;

  return readFirstFromMessage(desc, in);
}

static WirePackedData *
readNextFromPacked(const WirePackedData *packed)
{
  BOOST_ASSERT(0 != packed);

  // check if at the end
  if (packed->packed.second >= packed->content.second)
    return 0;

  Range packed_range(packed->packed.second, packed->packed.second);
  switch (typeToWireType(packed->field->type()))
  {
    case VARINT:
      lenVarInt(packed->content, packed_range);
      break;
    case FIXED32:
      lenFixed32(packed->content, packed_range);
      break;
    case FIXED64:
      lenFixed64(packed->content, packed_range);
      break;
    default:
      // should never happen
      BOOST_ASSERT(false);
      break;
  }

  return new WirePackedData(packed->field, packed->tag, packed->content, packed->size, packed_range);
}

static WireBaseData *
readFirstChild(const WireBaseData *data)
{
  WireBaseData *child(0);

  switch (data->type)
  {
    case WireBaseData::ROOT:
      {
        const WireRootData *root(static_cast<const WireRootData *>(data));
        if(root->content.first != root->content.second){
          child = readFirstFromMessage(root->message, root->content);
        }
      }
      break;
    case WireBaseData::MESSAGE_FIELD:
      {
        const WireMessageData *msg(static_cast<const WireMessageData *>(data));
        if(msg->content.first != msg->content.second){
          child = readFirstFromMessage(msg->message, msg->content);
        }
      }
      break;
      // do not have children
    case WireBaseData::ATOMIC_FIELD:
    case WireBaseData::LEN_DELIM_FIELD:
    case WireBaseData::PACKED_FIELD:
      // special end of stream marker
    case WireBaseData::EOS_MARKER:
      break;
  }

  // DEBUG
  printData(__PRETTY_FUNCTION__, child, std::cout);

  return child;
}

static WireBaseData *
readNextSibling(const WireBaseData *parent, const WireBaseData *data)
{
  BOOST_ASSERT(0 != parent); BOOST_ASSERT(0 != data);
  BOOST_ASSERT((parent->type == WireBaseData::MESSAGE_FIELD) || (parent->type == WireBaseData::ROOT));

  WireBaseData *next(0);

  switch (data->type)
  {
    case WireBaseData::ATOMIC_FIELD:
      {
        const WireAtomicData *atom(static_cast<const WireAtomicData *>(data));

        next = readNextFromMessage(parent, atom->content.second);
      }
      break;
    case WireBaseData::LEN_DELIM_FIELD:
      {
        const WireLenDelimData *lenDelim(static_cast<const WireLenDelimData *>(data));

        next = readNextFromMessage(parent, lenDelim->content.second);
      }
      break;
    case WireBaseData::MESSAGE_FIELD:
      {
        const WireMessageData *msg(static_cast<const WireMessageData *>(data));

        next = readNextFromMessage(parent, msg->content.second);
      }
      break;
    case WireBaseData::PACKED_FIELD:
      {
        const WirePackedData *packed(static_cast<const WirePackedData *>(data));

        next = readNextFromPacked(packed);
        if (0 == next)
          next = readNextFromMessage(parent, packed->content.second);
      }
      break;
    default:
      // should never happen
      BOOST_ASSERT(false);
      break;
  }

  // DEBUG
  printData(__PRETTY_FUNCTION__, next, std::cout);

  return next;
}

// ----------------------------------------------------------------------------
// WireNode implementation
// ----------------------------------------------------------------------------

WireNode::~WireNode()
{
  delete data_;
}

void *
WireNode::getInterface(const char *name) const
{
  if (wirenode_string == name)
    return (void *) this;
  return 0;
}

PQNode::NodeType
WireNode::getType() const
{
  switch (data_->type)
  {
    case WireBaseData::ATOMIC_FIELD:
    case WireBaseData::LEN_DELIM_FIELD:
    case WireBaseData::PACKED_FIELD:
      return FIELD;
    case WireBaseData::ROOT:
      return ROOT;
    case WireBaseData::MESSAGE_FIELD:
      return MESSAGE;
    default:
      // should never been reached
      throw PQTHROW("WireNode::getType()", "unknown node type!");
  }
}

PQNode::Ptr
WireNode::root() const
{
  WireNode::Ptr root(shared_from_this());

  while (WireBaseData::ROOT != root->data_->type)
    root = root->parent_;

  return root;
}

PQNode::Ptr
WireNode::parent() const
{
  return parent_;
}

PQNode::Ptr
WireNode::firstChild() const
{
  if ((WireBaseData::ROOT == data_->type) || (WireBaseData::MESSAGE_FIELD == data_->type))
  {
    WireBaseData* child = readFirstChild(data_);
    if(child){
      return ::boost::make_shared<WireNode>(shared_from_this(), readFirstChild(data_));
    }
  }
  return PQNode::Ptr();
}

PQNode::Ptr
WireNode::nextSibling() const
{
  if (WireBaseData::ROOT == data_->type)
    return PQNode::Ptr();
  WireBaseData *next(readNextSibling(static_cast<WireMessageData *>(parent_->data_), data_));
  if (0 != next)
    return ::boost::make_shared<WireNode>(parent_, shared_from_this(), next);
  return PQNode::Ptr();
}

PQNode::Ptr
WireNode::previousSibling() const
{
  return prev_;
}

AtomicValue getValueAtomic(const WireAtomicData* atom){
  AtomicValue value;
  Range range(atom->content);
  if (0 == atom->field)
  {
    value = AtomicValue(readVarInt(atom->content, range));
  }
  else
  {
    switch (atom->field->type())
    {
      case FieldDescriptor::TYPE_BOOL:
        {
          uint64 v(readVarInt(atom->content, range));
          if (v == 0)
            value = AtomicValue(false, FieldDescriptor::TYPE_BOOL);
          else
            value = AtomicValue(true, FieldDescriptor::TYPE_BOOL);
        }
        break;
      case FieldDescriptor::TYPE_INT32:
        {
          uint64 v(readVarInt(atom->content, range));
          value = AtomicValue(static_cast<int32>(v), FieldDescriptor::TYPE_INT32);
        }
        break;
      case FieldDescriptor::TYPE_UINT32:
        {
          uint64 v(readVarInt(atom->content, range));
          value = AtomicValue(static_cast<uint32>(v), FieldDescriptor::TYPE_UINT32);
        }
        break;
      case FieldDescriptor::TYPE_SINT32:
        {
          uint64 v(readVarInt(atom->content, range));
          value = AtomicValue(static_cast<int32>(zigzag(v)), FieldDescriptor::TYPE_SINT32);
        }
        break;
      case FieldDescriptor::TYPE_INT64:
        {
          uint64 v(readVarInt(atom->content, range));
          value = AtomicValue(static_cast<int64>(v), FieldDescriptor::TYPE_INT64);
        }
        break;
      case FieldDescriptor::TYPE_UINT64:
        {
          uint64 v(readVarInt(atom->content, range));
          value = AtomicValue(v, FieldDescriptor::TYPE_UINT64);
        }
        break;
      case FieldDescriptor::TYPE_SINT64:
        {
          uint64 v(readVarInt(atom->content, range));
          value = AtomicValue(zigzag(v), FieldDescriptor::TYPE_SINT64);
        }
        break;
      case FieldDescriptor::TYPE_FIXED32:
        value = AtomicValue(readFixed32(atom->content, range), FieldDescriptor::TYPE_FIXED32);
        break;
      case FieldDescriptor::TYPE_SFIXED32:
        value = AtomicValue(static_cast<int32>(readFixed32(atom->content, range)), FieldDescriptor::TYPE_SFIXED32);
        break;
      case FieldDescriptor::TYPE_FIXED64:
        value = AtomicValue(readFixed64(atom->content, range), FieldDescriptor::TYPE_FIXED32);
        break;
      case FieldDescriptor::TYPE_SFIXED64:
        value = AtomicValue(static_cast<int64>(readFixed64(atom->content, range)), FieldDescriptor::TYPE_SFIXED64);
        break;
      case FieldDescriptor::TYPE_FLOAT:
        value = AtomicValue(readFloat(atom->content, range), FieldDescriptor::TYPE_FLOAT);
        break;
      case FieldDescriptor::TYPE_DOUBLE:
        value = AtomicValue(readDouble(atom->content, range), FieldDescriptor::TYPE_DOUBLE);
        break;
      case FieldDescriptor::TYPE_ENUM:
        {
          uint64 v(readVarInt(atom->content, range));
          const EnumValueDescriptor *enum_value = atom->field->enum_type()->FindValueByNumber(static_cast<int>(v));
          if (0 == enum_value)
            value = AtomicValue(v, FieldDescriptor::TYPE_ENUM);
          else
            value = AtomicValue(enum_value, FieldDescriptor::TYPE_ENUM);
        }
        break;
      default:
        // should not been reached
        BOOST_ASSERT(false);
    }
  }
  return value;
}

AtomicValue getValueLenDelim(const WireLenDelimData* lenDelim) {
  AtomicValue value;
  Range range(lenDelim->content);
  ::std::string str(readString(lenDelim->content, range));

  if (0 == lenDelim->field)
  {
    value = AtomicValue(str, FieldDescriptor::TYPE_BYTES);
  }
  else
  {
    value = AtomicValue(str, lenDelim->field->type());
  }
  return value;
}


AtomicValue getValuePacked(const WirePackedData* packed) {
  AtomicValue value;

  Range range(packed->packed);
  // no need to check for 0 == field. Wouldn't have known its packed
  switch (packed->field->type())
  {
    case FieldDescriptor::TYPE_BOOL:
      {
        uint64 v(readVarInt(packed->packed, range));
        if (v == 0)
          value = AtomicValue(false, FieldDescriptor::TYPE_BOOL);
        else
          value = AtomicValue(true, FieldDescriptor::TYPE_BOOL);
      }
      break;
    case FieldDescriptor::TYPE_INT32:
      {
        uint64 v(readVarInt(packed->packed, range));
        value = AtomicValue(static_cast<int32>(v), FieldDescriptor::TYPE_INT32);
      }
      break;
    case FieldDescriptor::TYPE_UINT32:
      {
        uint64 v(readVarInt(packed->packed, range));
        value = AtomicValue(static_cast<uint32>(v), FieldDescriptor::TYPE_UINT32);
      }
      break;
    case FieldDescriptor::TYPE_SINT32:
      {
        uint64 v(readVarInt(packed->packed, range));
        value = AtomicValue(static_cast<int32>(zigzag(v)), FieldDescriptor::TYPE_SINT32);
      }
      break;
    case FieldDescriptor::TYPE_INT64:
      {
        uint64 v(readVarInt(packed->packed, range));
        value = AtomicValue(static_cast<int64>(v), FieldDescriptor::TYPE_INT64);
      }
      break;
    case FieldDescriptor::TYPE_UINT64:
      {
        uint64 v(readVarInt(packed->packed, range));
        value = AtomicValue(v, FieldDescriptor::TYPE_UINT64);
      }
      break;
    case FieldDescriptor::TYPE_SINT64:
      {
        uint64 v(readVarInt(packed->packed, range));
        value = AtomicValue(zigzag(v), FieldDescriptor::TYPE_SINT64);
      }
      break;
    case FieldDescriptor::TYPE_FIXED32:
      value = AtomicValue(readFixed32(packed->packed, range), FieldDescriptor::TYPE_FIXED32);
      break;
    case FieldDescriptor::TYPE_SFIXED32:
      value = AtomicValue(static_cast<int32>(readFixed32(packed->packed, range)), FieldDescriptor::TYPE_SFIXED32);
      break;
    case FieldDescriptor::TYPE_FIXED64:
      value = AtomicValue(readFixed64(packed->packed, range), FieldDescriptor::TYPE_FIXED32);
      break;
    case FieldDescriptor::TYPE_SFIXED64:
      value = AtomicValue(static_cast<int64>(readFixed64(packed->packed, range)), FieldDescriptor::TYPE_SFIXED64);
      break;
    case FieldDescriptor::TYPE_FLOAT:
      value = AtomicValue(readFloat(packed->packed, range), FieldDescriptor::TYPE_FLOAT);
      break;
    case FieldDescriptor::TYPE_DOUBLE:
        value = AtomicValue(readDouble(packed->packed, range), FieldDescriptor::TYPE_DOUBLE);
      break;
    case FieldDescriptor::TYPE_ENUM:
      {
        uint64 v(readVarInt(packed->packed, range));
        const EnumValueDescriptor *enum_value = packed->field->enum_type()->FindValueByNumber(static_cast<int>(v));
        if (0 == enum_value)
          value = AtomicValue(v, FieldDescriptor::TYPE_ENUM);
        else
          value = AtomicValue(enum_value, FieldDescriptor::TYPE_ENUM);
      }
      break;
    default:
      // should not been reached
      BOOST_ASSERT(false);
  }
  return value;
}

bool
WireNode::getValue(AtomicValue &value) const
{
  switch (data_->type)
  {
    case WireBaseData::ATOMIC_FIELD:
      {
        const WireAtomicData *atom(static_cast<const WireAtomicData *>(data_));
        value = getValueAtomic(atom);
      }
      return true;
    case WireBaseData::LEN_DELIM_FIELD:
      {
        const WireLenDelimData *lenDelim(static_cast<const WireLenDelimData *>(data_));
        value = getValueLenDelim(lenDelim);
      }
      return true;
    case WireBaseData::PACKED_FIELD:
      {
        const WirePackedData *packed(static_cast<const WirePackedData *>(data_));
        value = getValuePacked(packed);
      }
      return true;
    default:
      // other types don't have value
      break;
  }
  return false;
}

const Descriptor *
WireNode::getDescriptor() const
{
  switch (data_->type)
  {
    case WireBaseData::LEN_DELIM_FIELD:
    case WireBaseData::PACKED_FIELD:
    case WireBaseData::ATOMIC_FIELD:
      return 0;
    case WireBaseData::MESSAGE_FIELD:
      {
        const WireMessageData *msg(static_cast<const WireMessageData *>(data_));
        return msg->message;
      }
    case WireBaseData::ROOT:
      {
        const WireRootData *root(static_cast<const WireRootData *>(data_));
        return root->message;
      }
    default:
      // should never happen
      BOOST_ASSERT(false);
  }
  return 0;
}

const FieldDescriptor *
WireNode::getFieldDescriptor() const
{
  switch (data_->type)
  {
    case WireBaseData::ATOMIC_FIELD:
      {
        const WireAtomicData *atom(static_cast<const WireAtomicData *>(data_));
        return atom->field;
      }
    case WireBaseData::MESSAGE_FIELD:
      {
        const WireMessageData *msg(static_cast<const WireMessageData *>(data_));
        return msg->field;
      }
    case WireBaseData::PACKED_FIELD:
      {
        const WirePackedData *packed(static_cast<const WirePackedData *>(data_));
        return packed->field;
      }
    case WireBaseData::LEN_DELIM_FIELD:
      {
        const WireLenDelimData *lenDelim(static_cast<const WireLenDelimData *>(data_));
        return lenDelim->field;
      }
    case WireBaseData::ROOT:
      return 0;
    default:
      // should never happen
      BOOST_ASSERT(false);
  }
  return 0;
}

bool
WireNode::equals(const PQNode::Ptr &other) const
{
  const WireNode *node = reinterpret_cast<const WireNode *>(other->getInterface(wirenode_string));
  if (0 == node)
    return false;

  if (data_->type != node->data_->type)
    return false;

  switch (node->data_->type)
  {
    case WireBaseData::ROOT:
      {
        const WireRootData *oroot = reinterpret_cast<const WireRootData *>(node->data_);
        const WireRootData *root = reinterpret_cast<const WireRootData *>(data_);

        if (oroot->message != root->message)
          return false;
        if (oroot->string != root->string)
          return false;
        if (oroot->content != root->content)
          return false;
      }
      return true;
    case WireBaseData::ATOMIC_FIELD:
      {
        const WireAtomicData *oatom = reinterpret_cast<const WireAtomicData *>(node->data_);
        const WireAtomicData *atom = reinterpret_cast<const WireAtomicData *>(data_);

        if (oatom->field != atom->field)
          return false;
        if (oatom->tag != atom->tag)
          return false;
        if (oatom->content != atom->content)
          return false;
      }
      return true;
    case WireBaseData::LEN_DELIM_FIELD:
      {
        const WireLenDelimData *olenDelim = reinterpret_cast<const WireLenDelimData *>(node->data_);
        const WireLenDelimData *lenDelim = reinterpret_cast<const WireLenDelimData *>(data_);

        if (olenDelim->field != lenDelim->field)
          return false;
        if (olenDelim->tag != lenDelim->tag)
          return false;
        if (olenDelim->content != lenDelim->content)
          return false;
        if (olenDelim->size != olenDelim->size)
          return false;
      }
      return true;
    case WireBaseData::MESSAGE_FIELD:
      {
        const WireMessageData *omsg = reinterpret_cast<const WireMessageData *>(node->data_);
        const WireMessageData *msg = reinterpret_cast<const WireMessageData *>(data_);

        if (omsg->field != msg->field)
          return false;
        if (omsg->tag != msg->tag)
          return false;
        if (omsg->content != msg->content)
          return false;
        if (omsg->size != msg->size)
          return false;
        if (omsg->message != msg->message)
          return false;
      }
      return true;
    case WireBaseData::PACKED_FIELD:
      {
        const WirePackedData *opacked = reinterpret_cast<const WirePackedData *>(node->data_);
        const WirePackedData *packed = reinterpret_cast<const WirePackedData *>(data_);

        if (opacked->field != packed->field)
          return false;
        if (opacked->tag != packed->tag)
          return false;
        if (opacked->content != packed->content)
          return false;
        if (opacked->size != packed->size)
          return false;
        if (opacked->packed != packed->packed)
          return false;
      }
      return true;
    default:
      break;
  }
  return false;
}

static void
printDataDebug(std::string fn, const WireBaseData *data, std::ostream& stream)
{
  std::string unknown = "_unknown_";
  if (0 == data)
  {
    stream << fn << "_null_" << "\n";
    return;
  }
  switch (data->type)
  {
    case WireBaseData::ROOT:
      {
        const WireRootData *root(static_cast<const WireRootData *>(data));
        WireBaseData* child = readFirstChild(root);
        while (child) {
          printDataDebug(fn + INDENT,child,stream);
          child = readNextSibling(root,child);
        }
      }
      break;
    case WireBaseData::MESSAGE_FIELD:
      {
        const WireMessageData *msg(static_cast<const WireMessageData *>(data));
        std::string name = unknown;
        if (0 != msg->field) {
          name = msg->field->name();
        }
        stream << fn << name << ": {\n";
        WireBaseData* child = readFirstChild(data);
        while (child) {
          printDataDebug(fn + INDENT,child,stream);
          child = readNextSibling(data,child);
        }
        stream << fn << "}\n";
      }
      break;
    case WireBaseData::ATOMIC_FIELD:
      {
        const WireAtomicData *atom(static_cast<const WireAtomicData *>(data));
        std::string name = unknown;
        if (0 != atom->field) {
          name = atom->field->name();
        }
        stream << fn << name << ": " << getValueAtomic(atom).asString() << "\n";
      }
      break;
    case WireBaseData::LEN_DELIM_FIELD:
      {
        const WireLenDelimData *lenDelim(static_cast<const WireLenDelimData *>(data));
        std::string name = unknown;
        if (0 != lenDelim->field) {
          name = lenDelim->field->name();
        }
        AtomicValue value = getValueLenDelim(lenDelim);
        if(value.getType() != FieldDescriptor::TYPE_BYTES){
          stream << fn << name << ": " << value.asString() << "\n";
        } else {
          stream << fn << name << ": BYTES[" << lenDelim->content.second - lenDelim->content.first << "]\n";
        }
      }
      break;
    case WireBaseData::PACKED_FIELD:
      {
        const WirePackedData *packed(static_cast<const WirePackedData *>(data));
        std::string name = unknown;
        if (0 != packed->field) {
          name = packed->field->name();
        }
        AtomicValue value = getValuePacked(packed);
        if(value.getType() != FieldDescriptor::TYPE_BYTES){
          stream << fn << name << ": " << value.asString() << "\n";
        } else {
          stream << fn << name << ": PACKED_BYTES[" << packed->content.second - packed->content.first << "]\n";
        }
      }
      break;
    default:
      BOOST_ASSERT(false);
  }
}

std::string WireNode::DebugString() const {
  std::stringstream str;
  WireMessageData* data = static_cast<WireMessageData *>(data_);
  str << "pb:." << data->field->full_name() << "\n";
  printDataDebug("", data, str);
  return str.str();
}

// ----------------------------------------------------------------------------
// WireNodeFactoy implementation
// ----------------------------------------------------------------------------

WireNodeFactory::WireNodeFactory() {}

PQNode::Ptr
WireNodeFactory::createNode(const Descriptor *desc, ::std::string *str)
{
  WireRootData *root = new WireRootData(desc, str);
  return ::boost::make_shared<WireNode>(root);
}

PQNode::Ptr
WireNodeFactory::createNode(Message *message)
{
  // TODO: to be implemented
  return PQNode::Ptr();
}

}
}
