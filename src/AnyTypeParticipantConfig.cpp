/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/AnyTypeParticipantConfig.cpp                       **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <pquery/AnyTypeParticipantConfig.hpp>

#include <rsb/Factory.h>
#include <rsb/converter/Converter.h>
#include <rsb/converter/ConverterSelectionStrategy.h>
#include <rsb/converter/PredicateConverterList.h>
#include <rsb/converter/SchemaAndByteArrayConverter.h>

using namespace pquery;
using namespace rsb;
using namespace rsb::converter;


template <typename WireType>
typename ConverterSelectionStrategy<WireType>::Ptr createConverterSelectionStrategy() {
  // typedef for readability
  typedef std::pair<ConverterPredicatePtr, typename Converter<WireType>::Ptr> ConverterWirePair;

  // Construct a converter which converts everything to SchemaAndByteArray messages
  std::list<ConverterWirePair> converters;
  converters.push_back(
        ConverterWirePair(
          ConverterPredicatePtr(new AlwaysApplicable()),
          typename Converter<WireType>::Ptr(new SchemaAndByteArrayConverter())
          )
        );

  // Return a pointer to the constructed converter selection strategy.
  return typename ConverterSelectionStrategy<WireType>::Ptr(
        new PredicateConverterList<WireType>(converters.begin(), converters.end())
        );
}

AnyTypeParticipantConfig::AnyTypeParticipantConfig(const rsb::ParticipantConfig& config)
  : ParticipantConfig(config)
{
  init();
}

AnyTypeParticipantConfig::AnyTypeParticipantConfig()
  : ParticipantConfig(getFactory().getDefaultParticipantConfig())
{
  init();
}

void AnyTypeParticipantConfig::init(){
  // iterate over all transports
  std::set<ParticipantConfig::Transport> transports = getTransports();
  std::set<ParticipantConfig::Transport>::const_iterator it;
  for(it = transports.begin(); it != transports.end(); ++it) {
      Transport& transport = mutableTransport(it->getName());
      rsc::runtime::Properties options = transport.getOptions();
      options["converters"] = createConverterSelectionStrategy<std::string>();
      transport.setOptions(options);
    }
}
