/*
 * Copyright (c) 2012 Michael Beuse <mbeuse@techfak.uni-bielefeld.de>
 *
 * This file is part of PQuery.
 *
 * PQuery is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * PQuery is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with PQuery.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $Id$
 */
#include <pquery/pquery.hpp>

#include <pquery/pqueryImpl.hpp>
#include <pquery/WireNode.hpp>

#include <pquery/XQStringPool.hpp>
#include <pquery/XQDocumentCache.hpp>

#include <xercesc/validators/schema/SchemaSymbols.hpp>

#include <xqilla/context/DynamicContext.hpp>
#include <xqilla/utils/XQillaPlatformUtils.hpp>
#include <xqilla/simple-api/XQilla.hpp>
#include <xqilla/utils/XStr.hpp>
#include <xqilla/exceptions/XQException.hpp>
#include <xqilla/exceptions/XPath2ErrorException.hpp>
#include <xqilla/utils/UTF8Str.hpp>


#include <boost/make_shared.hpp>

namespace pquery
{

XQStringPool *
PQuery::fgStringPool = 0;

PQuery::PQuery()
{
  XQillaPlatformUtils::initialize();

  fgStringPool = new XQStringPool(XERCES_CPP_NAMESPACE_QUALIFIER XMLPlatformUtils::fgMemoryManager);
}

PQuery::~PQuery()
{
  XQillaPlatformUtils::terminate();
}

Item::Ptr
PQuery::getMessage(const Descriptor *descriptor, std::string *bytes)
{
  io::WireNodeFactory wf;

  return ::boost::make_shared<MessageImpl>(wf.createNode(descriptor, bytes));
}

Query::Ptr
PQuery::parse(const std::string &xpath)
{
  DynamicContext *context = XQilla::createContext(XQilla::XPATH2);

  context->setDocumentCache(new XQDocumentCache(context->getDocumentCache()));
  context->setNamespaceBinding(X("pbt"), XQStringPool::fgURI_PQUERY);
  context->setNamespaceBinding(X("xs"), XERCES_CPP_NAMESPACE_QUALIFIER SchemaSymbols::fgURI_SCHEMAFORSCHEMA);
  context->setNodeSetOrdering(StaticContext::ORDERING_UNORDERED);

  try
  {
    XQQuery *query = XQilla::parse(X(xpath.c_str()), context);

    return ::boost::make_shared<QueryImpl>(context, query, xpath);
  } catch (XQException &e)
  {
    throw PQTHROW("PQuery::parse()", UTF8(e.getError()));
  }
}

using ::pquery::Message;

Item::Type
Message::getType() const
{
  return MESSAGE;
}

Item::Type
Field::getType() const
{
  return FIELD;
}

Item::Type
Atomic::getType() const
{
  return ATOMIC;
}

Result &
Result::operator=(const Result &o)
{
  if (impl_ != o.impl_)
    impl_ = o.impl_;
  return *this;
}

Item::Ptr
Result::next()
{
  return impl_->next();
}

}
