/*
 * Copyright (c) 2012 Michael Beuse <mbeuse@techfak.uni-bielefeld.de>
 *
 * This file is part of PQuery.
 *
 * PQuery is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * PQuery is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with PQuery.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $Id$
 */
#include <pquery/AtomicValue.hpp>

namespace pquery
{

using ::google::protobuf::FieldDescriptor;
using ::google::protobuf::EnumValueDescriptor;
using ::std::ios_base;
using ::std::string;

AtomicValue::AtomicValue()
{}

AtomicValue::AtomicValue(bool value, FieldDescriptor::Type type) :
    type_(type), string_value_(ios_base::out)
{
  atomic_value_.bool_value = value;
  updateStringValue();
}

AtomicValue::AtomicValue(int32 value, FieldDescriptor::Type type) :
    type_(type), string_value_(ios_base::out)
{
  atomic_value_.int32_value = value;
  updateStringValue();
}

AtomicValue::AtomicValue(uint32 value, FieldDescriptor::Type type) :
    type_(type), string_value_(ios_base::out)
{
  atomic_value_.uint32_value = value;
  updateStringValue();
}

AtomicValue::AtomicValue(int64 value, FieldDescriptor::Type type) :
    type_(type), string_value_(ios_base::out)
{
  atomic_value_.int64_value = value;
  updateStringValue();
}

AtomicValue::AtomicValue(uint64 value, FieldDescriptor::Type type) :
    type_(type), string_value_(ios_base::out)
{
  atomic_value_.uint64_value = value;
  updateStringValue();
}

AtomicValue::AtomicValue(float value, FieldDescriptor::Type type) :
    type_(type), string_value_(ios_base::out)
{
  atomic_value_.float_value = value;
  updateStringValue();
}
AtomicValue::AtomicValue(double value, FieldDescriptor::Type type) :
    type_(type), string_value_(ios_base::out)
{
  atomic_value_.double_value = value;
  updateStringValue();
}

AtomicValue::AtomicValue(const std::string &value, FieldDescriptor::Type type) :
    type_(type), string_value_(value, ios_base::out)
{}

AtomicValue::AtomicValue(const uint8 *bytes, size_t len, FieldDescriptor::Type type) :
    type_(type), string_value_(string(reinterpret_cast<const char *>(bytes), len), ios_base::out)
{}

AtomicValue::AtomicValue(const EnumValueDescriptor *value, FieldDescriptor::Type type) :
    type_(type), string_value_(ios_base::out)
{
  atomic_value_.enum_value = value;
  updateStringValue();
}

AtomicValue::AtomicValue(const AtomicValue &other) :
    type_(other.type_), atomic_value_(other.atomic_value_),
    string_value_(other.string_value_.str(), ios_base::out)
{}

AtomicValue &
AtomicValue::operator=(const AtomicValue &other)
{
  if (this != &other)
  {
    type_         = other.type_;
    atomic_value_ = other.atomic_value_;
    string_value_ << other.string_value_.str();
  }
  return *this;
}

void
AtomicValue::updateStringValue()
{
  switch (type_)
  {
    case FieldDescriptor::TYPE_INT32:
    case FieldDescriptor::TYPE_SINT32:
    case FieldDescriptor::TYPE_SFIXED32:
      string_value_ << atomic_value_.int32_value;
      break;
    case FieldDescriptor::TYPE_INT64:
    case FieldDescriptor::TYPE_SINT64:
    case FieldDescriptor::TYPE_SFIXED64:
      string_value_ << atomic_value_.int64_value;
      break;
    case FieldDescriptor::TYPE_UINT32:
    case FieldDescriptor::TYPE_FIXED32:
      string_value_ << atomic_value_.uint32_value;
      break;
    case FieldDescriptor::TYPE_UINT64:
    case FieldDescriptor::TYPE_FIXED64:
      string_value_ << atomic_value_.uint64_value;
      break;
    case FieldDescriptor::TYPE_BOOL:
      string_value_ << atomic_value_.bool_value;
      break;
    case FieldDescriptor::TYPE_FLOAT:
      string_value_ << atomic_value_.float_value;
      break;
    case FieldDescriptor::TYPE_DOUBLE:
      string_value_ << atomic_value_.double_value;
      break;
    case FieldDescriptor::TYPE_ENUM:
      string_value_ << atomic_value_.enum_value->name();
      break;
    case FieldDescriptor::TYPE_STRING:
    case FieldDescriptor::TYPE_BYTES:
      // don't need conversion
      break;
    default:
      // other types are unsupported
      PQTHROW("AtomicValue::updateStringValue()", "Unsupported value type!");
  }
}

std::string
AtomicValue::asString() const
{
  return string_value_.str();
}

std::ostream &
operator<<(std::ostream &stream, const AtomicValue &value)
{
  stream << value.string_value_.str();
  return stream;
}

AtomicValue::operator bool() const
{
  return atomic_value_.bool_value;
}

AtomicValue::CppType
AtomicValue::getCppType() const
{
  switch (type_)
  {
    case FieldDescriptor::TYPE_INT32:
    case FieldDescriptor::TYPE_SINT32:
    case FieldDescriptor::TYPE_SFIXED32:
      return INT32;
    case FieldDescriptor::TYPE_INT64:
    case FieldDescriptor::TYPE_SINT64:
    case FieldDescriptor::TYPE_SFIXED64:
      return INT64;
    case FieldDescriptor::TYPE_UINT32:
    case FieldDescriptor::TYPE_FIXED32:
      return UINT32;
    case FieldDescriptor::TYPE_UINT64:
    case FieldDescriptor::TYPE_FIXED64:
      return UINT64;
    case FieldDescriptor::TYPE_BOOL:
      return BOOL;
    case FieldDescriptor::TYPE_FLOAT:
      return FLOAT;
    case FieldDescriptor::TYPE_DOUBLE:
      return DOUBLE;
    case FieldDescriptor::TYPE_ENUM:
    case FieldDescriptor::TYPE_STRING:
    case FieldDescriptor::TYPE_BYTES:
      return STRING;
    default:
      // other types are unsupported
      throw PQTHROW("AtomicValue::updateStringValue()", "Unsupported value type!");
  }
}

}
