/*
 * Copyright (c) 2012 Michael Beuse <mbeuse@techfak.uni-bielefeld.de>
 *
 * This file is part of PQuery.
 *
 * PQuery is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * PQuery is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with PQuery.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $Id$
 */
#include <pquery/XQNode.hpp>

#include <xercesc/util/XMLUniDefs.hpp>

#include <xqilla/context/DynamicContext.hpp>

#include <xqilla/runtime/Sequence.hpp>

namespace pquery
{

XERCES_CPP_NAMESPACE_USE;

const XMLCh
XQNode::xqnode_string[] = {
    chLatin_X,
    chLatin_Q,
    chLatin_X,
    chLatin_N,
    chLatin_o,
    chLatin_d,
    chLatin_e,
    chNull
};

Sequence
XQNode::dmBaseURI(const DynamicContext *context) const
{
  XQCALL;
  return Sequence(context->getMemoryManager());
}

Sequence
XQNode::dmDocumentURI(const DynamicContext *context) const
{
  XQCALL;
  return Sequence(context->getMemoryManager());
}

}
