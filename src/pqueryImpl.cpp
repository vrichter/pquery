/*
 * Copyright (c) 2012 Michael Beuse <mbeuse@techfak.uni-bielefeld.de>
 *
 * This file is part of PQuery.
 *
 * PQuery is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * PQuery is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with PQuery.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $Id$
 */
#include <pquery/pqueryImpl.hpp>
#include <pquery/XQNodeImpl.hpp>

#include <boost/make_shared.hpp>

#include <xqilla/simple-api/XQQuery.hpp>
#include <xqilla/context/DynamicContext.hpp>
#include <xqilla/utils/UTF8Str.hpp>

namespace pquery
{

static const std::string message_string_("message");
static const std::string unknown_string_("unknown");

const char
ItemImpl::itemImpl_string[] = "ItemImpl";

// ----------------------------------------------------------------------------
// MessageImpl
// ----------------------------------------------------------------------------

void *
MessageImpl::getInterface(const char *name) const
{
  if (ItemImpl::itemImpl_string == name)
    return (void *) this;
  return 0;
}

const std::string &
MessageImpl::getName() const
{
  const PQ_PROTOBUF_NS FieldDescriptor *field = node_->getFieldDescriptor();
  if (0 != field)
    return field->name();
  return message_string_;
}

std::string
MessageImpl::asString() const
{
  const PQ_PROTOBUF_NS FieldDescriptor *field = node_->getFieldDescriptor();
  if (0 != field)
    return field->full_name();
  return message_string_;
}

const PQ_PROTOBUF_NS Descriptor *
MessageImpl::getDescriptor() const
{
  return node_->getDescriptor();
}

PQ_PROTOBUF_NS Message *
MessageImpl::getMessage() const
{
  // TODO: to be implemented
  return NULL;
}

bool
MessageImpl::serialize(std::string &str) const
{
  return false;
}

// ----------------------------------------------------------------------------
// FieldImpl
// ----------------------------------------------------------------------------

void *
FieldImpl::getInterface(const char *name) const
{
  if (ItemImpl::itemImpl_string == name)
    return (void *) this;
  return 0;
}

const std::string &
FieldImpl::getName() const
{
  const PQ_PROTOBUF_NS FieldDescriptor *field = node_->getFieldDescriptor();
  if (0 != field)
    return field->name();
  return unknown_string_;
}

std::string
FieldImpl::asString() const
{
  return node_->getFieldDescriptor()->full_name();
}

const FieldDescriptor *
FieldImpl::getDescriptor() const
{
  return node_->getFieldDescriptor();
}

AtomicValue
FieldImpl::getValue() const
{
  AtomicValue v;
  node_->getValue(v);
  return v;
}

// ----------------------------------------------------------------------------
// AtomicImpl
// ----------------------------------------------------------------------------

void *
AtomicImpl::getInterface(const char *name) const
{
  if (ItemImpl::itemImpl_string == name)
    return (void *) this;
  return 0;
}

std::string
AtomicImpl::asString() const
{
  return value_.asString();
}

AtomicValue
AtomicImpl::getValue() const
{
  return value_;
}

// ----------------------------------------------------------------------------
// ResultImplImpl
// ----------------------------------------------------------------------------

Item::Ptr
ResultImplImpl::next()
{
  ::Item::Ptr next(result_->next(ctx_));

  if (next.notNull())
  {
    if (next->isAtomicValue())
    {
      AtomicValue value(UTF8(next->asString(ctx_)));
      return ::boost::make_shared<AtomicImpl>(value);
    }
    else
    {
      XQNodeImpl *item(reinterpret_cast<XQNodeImpl *>(next->getInterface(XQNode::xqnode_string)));

      io::PQNode::Ptr node(item->getPQNode());
      switch (node->getType())
      {
        case io::PQNode::ROOT:
        case io::PQNode::MESSAGE:
          return ::boost::make_shared<MessageImpl>(node);
        case io::PQNode::FIELD:
          return ::boost::make_shared<FieldImpl>(node);
        default:
          break;
      }
    }
  }
  return Item::Ptr();
}

// ----------------------------------------------------------------------------
// QueryImpl
// ----------------------------------------------------------------------------

QueryImpl::~QueryImpl()
{
  delete xqquery_;
}

Result
QueryImpl::execute(const Item::Ptr &context)
{
  const ItemImpl *item = reinterpret_cast<const ItemImpl *>(context->getInterface(ItemImpl::itemImpl_string));
  if (0 == item)
    return Result();

  XQNodeImpl::Ptr ctxItem = new XQNodeImpl(item->getNode());

  ctx_->setContextItem(ctxItem);
  ctx_->setContextPosition(1);
  ctx_->setContextSize(1);

  ResultImplImpl::Ptr result = ::boost::make_shared<ResultImplImpl>(ctx_, xqquery_->execute(ctx_));

  return Result(result);
}

}
