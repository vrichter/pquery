/*
 * Copyright (c) 2012 Michael Beuse <mbeuse@techfak.uni-bielefeld.de>
 *
 * This file is part of PQuery.
 *
 * PQuery is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * PQuery is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with PQuery.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $Id$
 */
#include <pquery/XQNodeImpl.hpp>

#include <pquery/pquery.hpp>
#include <pquery/XQStringPool.hpp>

#include <boost/assert.hpp>

#include <xercesc/framework/XMLBuffer.hpp>
#include <xercesc/util/XMLUniDefs.hpp>
#include <xercesc/validators/schema/SchemaSymbols.hpp>

#include <xqilla/context/DynamicContext.hpp>
#include <xqilla/context/ItemFactory.hpp>
#include <xqilla/runtime/ResultImpl.hpp>
#include <xqilla/runtime/Sequence.hpp>
#include <xqilla/axis/NodeTest.hpp>
#include <xqilla/utils/XStr.hpp>
#include <xqilla/utils/XPath2Utils.hpp>

namespace pquery
{

using namespace ::google::protobuf;
XERCES_CPP_NAMESPACE_USE;

// ----------------------------------------------------------------------------
// XQNodeAxis
// ----------------------------------------------------------------------------

class XQNodeAxis : public ::ResultImpl
{
  protected:
    bool            toDo_;
    XQNodeImpl::Ptr contextNode_;
    const NodeTest *nodeTest_;

  public:
    XQNodeAxis(const XQNodeImpl::Ptr &contextNode, const NodeTest *nodeTest, const LocationInfo *info) :
      ::ResultImpl(info), toDo_(false), contextNode_(contextNode), nodeTest_(nodeTest) {}

    virtual ::Item::Ptr next(DynamicContext *context);

    virtual XQNodeImpl::Ptr nextNode() = 0;
};

::Item::Ptr
XQNodeAxis::next(DynamicContext *context)
{
  XQNodeImpl::Ptr node;

  while (0 != (node = nextNode()))
  {
    if (0 == nodeTest_)
      break;
    if (nodeTest_->filterNode(node, context))
      return node;
  }

  return 0;
}

// ----------------------------------------------------------------------------
// XQNodeChildAxis
// ----------------------------------------------------------------------------

class XQNodeChildAxis : public XQNodeAxis
{
  protected:
    XQNodeImpl::Ptr cur_;
  public:
    XQNodeChildAxis(const XQNodeImpl::Ptr &contextNode, const NodeTest *nodeTest, const LocationInfo *info) :
      XQNodeAxis(contextNode, nodeTest, info)
    {
      toDo_ = true;
    }

    virtual XQNodeImpl::Ptr nextNode();
};

XQNodeImpl::Ptr
XQNodeChildAxis::nextNode()
{
  if (toDo_)
  {
    cur_ = contextNode_->firstChild();
    toDo_ = false;
  }
  else
  {
    cur_ = cur_->nextSibling();
  }

  return cur_;
}

// ----------------------------------------------------------------------------
// XQNodeFollowingSiblingAxis
// ----------------------------------------------------------------------------

class XQNodeFollowingSiblingAxis : public XQNodeAxis
{
  protected:
    XQNodeImpl::Ptr cur_;
  public:
    XQNodeFollowingSiblingAxis(const XQNodeImpl::Ptr &node, const NodeTest *nodeTest, const LocationInfo *info) :
      XQNodeAxis(node->nextSibling(), nodeTest, info), cur_(contextNode_)
    {}

    virtual XQNodeImpl::Ptr nextNode();
};

XQNodeImpl::Ptr
XQNodeFollowingSiblingAxis::nextNode()
{
  return (cur_ = cur_->nextSibling());
}

// ----------------------------------------------------------------------------
// XQNodePrecedingSiblingAxis
// ----------------------------------------------------------------------------

class XQNodePrecedingSiblingAxis : public XQNodeAxis
{
  protected:
    XQNodeImpl::Ptr cur_;
  public:
    XQNodePrecedingSiblingAxis(const XQNodeImpl::Ptr &node, const NodeTest *nodeTest, const LocationInfo *info) :
      XQNodeAxis(node->prevSibling(), nodeTest, info), cur_(contextNode_)
    {}

    virtual XQNodeImpl::Ptr nextNode();
};

XQNodeImpl::Ptr
XQNodePrecedingSiblingAxis::nextNode()
{
  return (cur_ = cur_->prevSibling());
}

// ----------------------------------------------------------------------------
// XQNodeDescendantAxis
// ----------------------------------------------------------------------------

class XQNodeDescendantAxis : public XQNodeAxis
{
  protected:
    XQNodeImpl::Ptr cur_;
  public:
    XQNodeDescendantAxis(const XQNodeImpl::Ptr &node, const NodeTest *nodeTest, const LocationInfo *info) :
      XQNodeAxis(node, nodeTest, info), cur_(contextNode_)
    {
      toDo_ = true;
    }

    virtual XQNodeImpl::Ptr nextNode();
};

XQNodeImpl::Ptr
XQNodeDescendantAxis::nextNode()
{
  if (toDo_)
  {
    toDo_ = false;
    cur_  = contextNode_->firstChild();
  }
  else if (0 != cur_)
  {
    XQNodeImpl::Ptr next(cur_->firstChild());

    while (0 == next)
    {
      next = cur_->nextSibling();
      if (0 == next)
      {
        cur_ = cur_->parent();
        if (cur_->getPQNode()->equals(contextNode_->getPQNode()))
        {
          next = 0;
          break;
        }
        else
        {
          next = cur_->nextSibling();
        }
      }
    }
    cur_ = next;
  }
  return cur_;
}

// ----------------------------------------------------------------------------
// XQNodeDescendantOrSelfAxis
// ----------------------------------------------------------------------------

class XQNodeDescendantOrSelfAxis : public XQNodeAxis
{
  protected:
    XQNodeImpl::Ptr cur_;
  public:
    XQNodeDescendantOrSelfAxis(const XQNodeImpl::Ptr &node, const NodeTest *nodeTest, const LocationInfo *info) :
      XQNodeAxis(node, nodeTest, info), cur_(contextNode_)
    {
      toDo_ = true;
    }

    virtual XQNodeImpl::Ptr nextNode();
};

XQNodeImpl::Ptr
XQNodeDescendantOrSelfAxis::nextNode()
{
  if (toDo_)
  {
    toDo_ = false;
    cur_  = contextNode_;
  }
  else if (0 != cur_)
  {
    XQNodeImpl::Ptr next(cur_->firstChild());

    while (0 == next)
    {
      next = cur_->nextSibling();
      if (0 == next)
      {
        cur_ = cur_->parent();
        if (cur_->getPQNode()->equals(contextNode_->getPQNode()))
        {
          next = 0;
          break;
        }
        else
        {
          next = cur_->nextSibling();
        }
      }
    }
    cur_ = next;
  }
  return cur_;
}

// ----------------------------------------------------------------------------
// XQNodeSelfAxis
// ----------------------------------------------------------------------------

class XQNodeSelfAxis : public XQNodeAxis
{
  public:
    XQNodeSelfAxis(const XQNodeImpl::Ptr &node, const NodeTest *nodeTest, const LocationInfo *info) :
      XQNodeAxis(node, nodeTest, info) { toDo_ = true; }

    virtual XQNodeImpl::Ptr nextNode();
};

XQNodeImpl::Ptr
XQNodeSelfAxis::nextNode()
{
  if (toDo_)
  {
    toDo_ = false;
    return contextNode_;
  }
  return 0;
}

// ----------------------------------------------------------------------------
// XQNodeImpl implementation
// ----------------------------------------------------------------------------

XQNodeImpl::XQNodeImpl(const io::PQNode::Ptr &node, Type type) : node_(node), type_(type)
{
  if ((ELEMENT == type) || (TYPE_ATTRIBUTE == type))
  {
    if (io::PQNode::ROOT == node_->getType())
    {
      PQuery::fgStringPool->getType(node_->getDescriptor(), typeURI_, typeName_);
      localName_ = XQStringPool::fgMESSAGE;
    }
    else
    {
      PQuery::fgStringPool->getType(node_->getFieldDescriptor(), typeURI_, typeName_);
      localName_ = PQuery::fgStringPool->getName(node_->getFieldDescriptor());
    }
  }
}

const XMLCh *
XQNodeImpl::asString(const DynamicContext *context) const
{
  XQCALL;

  XMLBuffer buf;
  typeToBuffer(const_cast<DynamicContext *>(context), buf);

  return context->getMemoryManager()->getPooledString(buf.getRawBuffer());
}

const XMLCh *
XQNodeImpl::getTypeURI() const
{
  XQCALL;
  return typeURI_;
}

const XMLCh *
XQNodeImpl::getTypeName() const
{
  XQCALL;
  return typeName_;
}

void *
XQNodeImpl::getInterface(const XMLCh *name) const
{
  XQCALL;
  if (xqnode_string == name)
    return (void *) this;
  return 0;
}

void
XQNodeImpl::generateEvents(EventHandler *events, const DynamicContext *context,
                       bool preserveNS, bool preseveType) const
{
  XQCALL;
  // TODO: to be implemented
}

bool
XQNodeImpl::hasInstanceOfType(const XMLCh *typeURI, const XMLCh *typeName, const DynamicContext *context) const
{
  XQCALL;
  if ((0 == typeURI_) || (0 == typeName_))
      return false;
  if (XPath2Utils::equals(typeURI_, typeURI) && XPath2Utils::equals(typeName_, typeName))
    return true;
  return false;
}

const XMLCh *
XQNodeImpl::dmNodeKind() const
{
  XQCALL;
  switch (type_)
  {
    case DOCUMENT:
      return document_string;
    case ELEMENT:
      return element_string;
    case TEXT:
      return text_string;
    case TYPE_ATTRIBUTE:
      return attribute_string;
    default:
      BOOST_ASSERT(false);
      break;
  }
  return 0;
}

ATQNameOrDerived::Ptr
XQNodeImpl::dmNodeName(const DynamicContext *context) const
{
  XQCALL;
  if (ELEMENT == type_)
    return context->getItemFactory()->createQName(XMLUni::fgZeroLenString, XMLUni::fgZeroLenString, localName_, context);
  if (TYPE_ATTRIBUTE == type_)
    return context->getItemFactory()->createQName(XMLUni::fgZeroLenString, XMLUni::fgZeroLenString, X("type"), context);
  return 0;
}

const XMLCh *
XQNodeImpl::dmStringValue(const DynamicContext *context) const
{
  XQCALL;
  if (TYPE_ATTRIBUTE == type_)
  {
    return typeName_;
  }
  else
  {
    AtomicValue value;
    if (!node_->getValue(value))
      return 0;
    return context->getMemoryManager()->getPooledString(value.asString().c_str());
  }
}

Sequence
XQNodeImpl::dmTypedValue(DynamicContext *context) const
{
  XQCALL;
  AtomicValue value;
  Sequence seq(context->getMemoryManager());

  if (TYPE_ATTRIBUTE == type_)
  {
    seq.addItem(context->getItemFactory()->createString(typeName_, context));
  }
  else if (node_->getValue(value))
  {
    switch (value.getCppType())
    {
      case AtomicValue::INT32:
      case AtomicValue::UINT32:
      case AtomicValue::INT64:
      case AtomicValue::UINT64:
        seq.addItem(context->getItemFactory()->createDecimal(X(value.asString().c_str()), context));
        break;
      case AtomicValue::BOOL:
        seq.addItem(context->getItemFactory()->createBoolean(value, context));
        break;
      case AtomicValue::FLOAT:
        seq.addItem(context->getItemFactory()->createFloat(X(value.asString().c_str()), context));
        break;
      case AtomicValue::DOUBLE:
        seq.addItem(context->getItemFactory()->createDouble(X(value.asString().c_str()), context));
        break;
      case AtomicValue::STRING:
        seq.addItem(context->getItemFactory()->createString(X(value.asString().c_str()), context));
        break;
      default:
        PQTHROW("XQNodeImpl::dmTypedValue()", "Unknown atomic type!");
    }
  }
  return seq;
}

ATQNameOrDerived::Ptr
XQNodeImpl::dmTypeName(const DynamicContext *context) const
{
  XQCALL;
  return context->getItemFactory()->createQName(typeURI_, XMLUni::fgZeroLenString, typeName_, context);
}

ATBooleanOrDerived::Ptr
XQNodeImpl::dmNilled(const DynamicContext *context) const
{
  XQCALL;
  return context->getItemFactory()->createBoolean(false, context);
}

bool
XQNodeImpl::lessThan(const Node::Ptr &other, const DynamicContext *context) const
{
  XQCALL;
  return true;
}

bool
XQNodeImpl::equals(const Node::Ptr &other) const
{
  XQCALL;
  const XQNodeImpl *node = reinterpret_cast<const XQNodeImpl *>(other->getInterface(xqnode_string));
  if (0 == node)
    return false;
  return node_->equals(node->getPQNode());
}

bool
XQNodeImpl::uniqueLessThan(const Node::Ptr &other, const DynamicContext *context) const
{
  XQCALL;
  return lessThan(other, context);
}

Node::Ptr
XQNodeImpl::root(const DynamicContext *context) const
{
  XQCALL;
  if (DOCUMENT == type_)
    return this;
  return new XQNodeImpl(node_->root(), DOCUMENT);
}

Node::Ptr
XQNodeImpl::dmParent(const DynamicContext *context) const
{
  XQCALL;
  return parent();
}

::Result
XQNodeImpl::dmAttributes(const DynamicContext *context, const LocationInfo *info) const
{
  XQCALL;
  return new XQNodeSelfAxis(new XQNodeImpl(node_, TYPE_ATTRIBUTE), 0, info);
}

::Result
XQNodeImpl::dmNamespaceNodes(const DynamicContext *context, const LocationInfo *info) const
{
  XQCALL;
  // TODO: to be implemented
  return 0;
}

::Result
XQNodeImpl::dmChildren(const DynamicContext *context, const LocationInfo *info) const
{
  XQCALL;
  return new XQNodeChildAxis(this, 0, info);
}

::Result
XQNodeImpl::getAxisResult(XQStep::Axis axis, const NodeTest *nodeTest, const DynamicContext *context, const LocationInfo *info) const
{
  XQCALL;

  switch (axis)
  {
    case XQStep::CHILD:
      return new XQNodeChildAxis(this, nodeTest, info);
    case XQStep::FOLLOWING_SIBLING:
      return new XQNodeFollowingSiblingAxis(this, nodeTest, info);
    case XQStep::PRECEDING_SIBLING:
      return new XQNodePrecedingSiblingAxis(this, nodeTest, info);
    case XQStep::DESCENDANT:
      return new XQNodeDescendantAxis(this, nodeTest, info);
    case XQStep::DESCENDANT_OR_SELF:
      return new XQNodeDescendantOrSelfAxis(this, nodeTest, info);
    case XQStep::SELF:
      return new XQNodeSelfAxis(this, nodeTest, info);
    case XQStep::ATTRIBUTE:
      return new XQNodeSelfAxis(new XQNodeImpl(node_, TYPE_ATTRIBUTE), nodeTest, info);
    default:
      BOOST_ASSERT_MSG(false, "Not yet implemented");
      break;
  }
  return 0;
}

ATBooleanOrDerived::Ptr
XQNodeImpl::dmIsId(const DynamicContext *context) const
{
  XQCALL;
  if ((ELEMENT == type_) || (TYPE_ATTRIBUTE == type_))
    return context->getItemFactory()->createBoolean(false, context);
  return 0;
}

ATBooleanOrDerived::Ptr
XQNodeImpl::dmIsIdRefs(const DynamicContext *context) const
{
  XQCALL;
  if ((ELEMENT == type_) || (TYPE_ATTRIBUTE == type_))
    return context->getItemFactory()->createBoolean(false, context);
  return 0;
}

const Descriptor *
XQNodeImpl::getDescriptor() const
{
  XQCALL;
  // TODO: to be implemented
  return 0;
}

const FieldDescriptor *
XQNodeImpl::getFieldDescriptor() const
{
  XQCALL;
  // TODO: to be implemented
  return 0;
}

bool
XQNodeImpl::getValue(AtomicValue &value) const
{
  XQCALL;
  return node_->getValue(value);
}

PQ_PROTOBUF_NS Message *
XQNodeImpl::getMessage() const
{
  XQCALL;
  // TODO: to be implemented
  return 0;
}

bool
XQNodeImpl::serialize(PQ_PROTOBUF_NS io::ZeroCopyOutputStream *ostream) const
{
  XQCALL;
  // TODO: to be implemented
  return false;
}

bool
XQNodeImpl::serialize(::std::string &str) const
{
  XQCALL;
  // TODO: to be implemented
  return false;
}

XQNodeImpl::Ptr
XQNodeImpl::firstChild() const
{
  switch (type_)
  {
    case DOCUMENT:
      return new XQNodeImpl(node_);
    case ELEMENT:
      if (io::PQNode::FIELD == node_->getType())
        return new XQNodeImpl(node_, TEXT);
      else
      {
        io::PQNode::Ptr node(node_->firstChild());
        if (node)
          return new XQNodeImpl(node, ELEMENT);
      }
    case TEXT:
      return 0;
    default:
      BOOST_ASSERT(false);
      break;
  }
  return 0;
}

XQNodeImpl::Ptr
XQNodeImpl::parent() const
{
  switch (type_)
  {
    case DOCUMENT:
      return 0;
    case ELEMENT:
      if (io::PQNode::ROOT == node_->getType())
        return new XQNodeImpl(node_, DOCUMENT);
      else
        return new XQNodeImpl(node_->parent());
    case TEXT:
    case TYPE_ATTRIBUTE:
      return new XQNodeImpl(node_);
    default:
      BOOST_ASSERT(false);
      break;
  }
  return 0;
}

XQNodeImpl::Ptr
XQNodeImpl::nextSibling() const
{
  switch (type_)
  {
    case DOCUMENT:
    case TEXT:
    case TYPE_ATTRIBUTE:
      return 0;
    case ELEMENT:
      {
        io::PQNode::Ptr node(node_->nextSibling());
        if (node)
          return new XQNodeImpl(node);
      }
      break;
    default:
      BOOST_ASSERT(false);
      break;
  }
  return 0;
}

XQNodeImpl::Ptr
XQNodeImpl::prevSibling() const
{
  switch (type_)
  {
    case DOCUMENT:
    case TEXT:
    case TYPE_ATTRIBUTE:
      return 0;
    case ELEMENT:
      {
        io::PQNode::Ptr node(node_->previousSibling());
        if (node)
          return new XQNodeImpl(node);
      }
      break;
    default:
      BOOST_ASSERT(false);
      break;
  }
  return 0;
}

io::PQNode::Ptr
XQNodeImpl::getPQNode() const
{
  return node_;
}

}
