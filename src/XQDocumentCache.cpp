/*
 * Copyright (c) 2012 Michael Beuse <mbeuse@techfak.uni-bielefeld.de>
 *
 * This file is part of PQuery.
 *
 * PQuery is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * PQuery is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with PQuery.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $Id$
 */
#include <pquery/XQDocumentCache.hpp>
#include <pquery/XQStringPool.hpp>

#include <xqilla/utils/UTF8Str.hpp>
#include <xqilla/utils/XPath2Utils.hpp>

namespace pquery
{

bool
XQDocumentCache::isTypeDefined(const XMLCh * const uri, const XMLCh * const typeName) const
{
  if (XPath2Utils::equals(XQStringPool::fgURI_PQUERY, uri))
    // perhaps add check to verify the specified type exists.
    return true;
  return wrapped_->isTypeDefined(uri, typeName);
}

}
