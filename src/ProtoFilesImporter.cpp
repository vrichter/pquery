/********************************************************************
**                                                                 **
** Copyright (C) 2014 Viktor Richter                               **
**                                                                 **
** File   : src/ProtoFilesImporter.cpp                             **
** Authors: Viktor Richter                                         **
**                                                                 **
**                                                                 **
** GNU LESSER GENERAL PUBLIC LICENSE                               **
** This file may be used under the terms of the GNU Lesser General **
** Public License version 3.0 as published by the                  **
**                                                                 **
** Free Software Foundation and appearing in the file LICENSE.LGPL **
** included in the packaging of this file.  Please review the      **
** following information to ensure the license requirements will   **
** be met: http://www.gnu.org/licenses/lgpl-3.0.txt                **
**                                                                 **
********************************************************************/

#include <google/protobuf/descriptor.h>
#include <boost/tokenizer.hpp>

#include <iostream>
#include <algorithm>

#include <pquery/ProtoFilesImporter.hpp>

using namespace pquery;
using namespace google::protobuf;
using namespace google::protobuf::compiler;

class BasicTypeSet : public std::set<std::string> {
public:
  BasicTypeSet(){
    insert("double");
    insert("float");
    insert("int32");
    insert("int64");
    insert("uint32");
    insert("uint64");
    insert("sint32");
    insert("sint64");
    insert("fixed32");
    insert("fixed64");
    insert("sfixed32");
    insert("sfixed64");
    insert("bool");
    insert("string");
    insert("bytes");
  }
};

void PrintingErrorCollector::AddError(const std::string& filename, int line, int column, const std::string& message){
  std::cout << "Encountered an error while parsing message definition."
            << "\n\tFilename: " << filename
            << "\n\tLine,Column: " << line << "," << column
            << "\n\tError: " << message << std::endl;
}

ProtoFilesImporter::ProtoFilesImporter(const std::string& path, const std::string delimiters)
  : m_SourceTree(), m_ErrorCollector(), m_Importer(&m_SourceTree, &m_ErrorCollector)
{
  boost::char_separator<char> separator(delimiters.c_str());
  boost::tokenizer<boost::char_separator<char> > tokens(path, separator);
  boost::tokenizer<boost::char_separator<char> >::iterator it;
  for(it = tokens.begin(); it != tokens.end(); ++it){
    const std::string& token = *it;
    if(!token.empty()){
      // add default rst,rst-experimental map paths
      m_SourceTree.MapPath("",token);
      m_SourceTree.MapPath("rst",token+std::string("/sandbox/rst"));
      m_SourceTree.MapPath("rst",token+std::string("/stable/rst"));
      m_SourceTree.MapPath("rst",token+std::string("/deprecated/rst"));
      m_SourceTree.MapPath("rst",token+std::string("/experimental/rst"));
    }
  }
}

const Descriptor* ProtoFilesImporter::findDescriptor(const std::string& typeName){
  // remove point at the begining of typeName (.rst.Type -> rst.Type)
  std::string name = typeName[0] == '.' ? typeName.substr(1) : typeName;

  // check whether already tried to import corresponding file
  if(m_Imported.find(typeName) == m_Imported.end()){
      // create file name from
      std::string fileName = name;
      std::replace(fileName.begin(),fileName.end(),'.','/');
      fileName += std::string(".proto");
      // import
      m_Importer.Import(fileName);
      // add to imported set
      m_Imported.insert(typeName);
    }

  // return what ever the pool can provide (NULL when not found)
  return m_Importer.pool()->FindMessageTypeByName(name);
}

bool ProtoFilesImporter::isBasicType(const std::string& type){
  const static BasicTypeSet bTypes;
  return bTypes.find(type) != bTypes.end();
}

