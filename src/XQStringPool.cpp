/*
 * Copyright (c) 2012 Michael Beuse <mbeuse@techfak.uni-bielefeld.de>
 *
 * This file is part of PQuery.
 *
 * PQuery is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * PQuery is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with PQuery.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $Id$
 */
#include <pquery/XQStringPool.hpp>

#include <boost/assert.hpp>

#include <google/protobuf/descriptor.h>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/XMLUniDefs.hpp>
#include <xercesc/validators/schema/SchemaSymbols.hpp>

namespace pquery
{

XERCES_CPP_NAMESPACE_USE;
using namespace ::google::protobuf;

const XMLCh
XQStringPool::fgURI_PQUERY[] = {
    chLatin_h,
    chLatin_t,
    chLatin_t,
    chLatin_p,
    chColon,
    chForwardSlash,
    chForwardSlash,
    chLatin_c,
    chLatin_o,
    chLatin_d,
    chLatin_e,
    chPeriod,
    chLatin_c,
    chLatin_o,
    chLatin_r,
    chDash,
    chLatin_l,
    chLatin_a,
    chLatin_b,
    chPeriod,
    chLatin_d,
    chLatin_e,
    chForwardSlash,
    chLatin_p,
    chLatin_q,
    chLatin_u,
    chLatin_e,
    chLatin_r,
    chLatin_y,
    chNull
};

const XMLCh
XQStringPool::fgMESSAGE[] = {
    chLatin_m,
    chLatin_e,
    chLatin_s,
    chLatin_s,
    chLatin_a,
    chLatin_g,
    chLatin_e,
    chNull
};

XQStringPool::XQStringPool(MemoryManager *memMgr) :
    memMgr_(memMgr)
{}

XQStringPool::~XQStringPool()
{
  clear();
}

void
XQStringPool::getType(const Descriptor *desc, const XMLCh *&uri, const XMLCh *&name)
{
  BOOST_ASSERT(0 != desc);

  // search pooled type
  TypeMap::const_iterator type = typePool_.find(desc);

  // if not set add it
  if (typePool_.end() == type)
  {
    uri  = fgURI_PQUERY;
    name = XMLString::transcode(desc->full_name().c_str(), memMgr_);

    typePool_[desc] = make_pair<const XMLCh *, const XMLCh *>(uri, name);
  }
  else
  {
    uri  = type->second.first;
    name = type->second.second;
  }
}

void
XQStringPool::getType(const FieldDescriptor *desc, const XMLCh *&uri, const XMLCh *&name)
{
  // search pooled type
  TypeMap::const_iterator type = typePool_.find(desc);

  // if not set add it
  if (typePool_.end() == type)
  {
    if(!desc){
      name = SchemaSymbols::fgDT_BASE64BINARY;
    } else {
      switch (desc->type())
      {
        case FieldDescriptor::TYPE_MESSAGE:
          getType(desc->message_type(), uri, name);
          return;
        case FieldDescriptor::TYPE_ENUM:
          getType(desc->enum_type(), uri, name);
          return;
        case FieldDescriptor::TYPE_BOOL:
          name = SchemaSymbols::fgDT_BOOLEAN;
          break;
        case FieldDescriptor::TYPE_INT32:
        case FieldDescriptor::TYPE_SINT32:
        case FieldDescriptor::TYPE_INT64:
        case FieldDescriptor::TYPE_SINT64:
          name = SchemaSymbols::fgDT_INTEGER;
          break;
        case FieldDescriptor::TYPE_UINT32:
        case FieldDescriptor::TYPE_UINT64:
          name = SchemaSymbols::fgDT_NONNEGATIVEINTEGER;
          break;
        case FieldDescriptor::TYPE_FIXED32:
          name = SchemaSymbols::fgDT_UINT;
          break;
        case FieldDescriptor::TYPE_SFIXED32:
          name = SchemaSymbols::fgDT_INT;
          break;
        case FieldDescriptor::TYPE_FIXED64:
          name = SchemaSymbols::fgDT_ULONG;
          break;
        case FieldDescriptor::TYPE_SFIXED64:
          name = SchemaSymbols::fgDT_LONG;
          break;
        case FieldDescriptor::TYPE_FLOAT:
          name = SchemaSymbols::fgDT_FLOAT;
          break;
        case FieldDescriptor::TYPE_DOUBLE:
          name = SchemaSymbols::fgDT_DOUBLE;
          break;
        case FieldDescriptor::TYPE_BYTES:
          name = SchemaSymbols::fgDT_BASE64BINARY;
          break;
        case FieldDescriptor::TYPE_STRING:
          name = SchemaSymbols::fgDT_STRING;
          break;
        case FieldDescriptor::TYPE_GROUP:
          PQTHROW("XQStringPool::getType()", "Groups are deprecated! Not supported!");
        default:
          PQTHROW("XQStringPool::getType()", "Unknown field type!");
      }
    }
    uri = SchemaSymbols::fgURI_SCHEMAFORSCHEMA;

    // do have to replicate name due to memory allocation model
    typePool_[desc] = make_pair<const XMLCh *, const XMLCh *>(uri, XMLString::replicate(name, memMgr_));
  }
  else
  {
    uri  = type->second.first;
    name = type->second.second;
  }
}

void
XQStringPool::getType(const EnumDescriptor *desc, const XMLCh *&uri, const XMLCh *&name)
{
  BOOST_ASSERT(0 != desc);

  // search pooled type
  TypeMap::const_iterator type = typePool_.find(desc);

  // if not set add it
  if (typePool_.end() == type)
  {
    uri  = fgURI_PQUERY;
    name = XMLString::transcode(desc->full_name().c_str(), memMgr_);

    typePool_[desc] = make_pair<const XMLCh *, const XMLCh *>(uri, name);
  }
  else
  {
    uri  = type->second.first;
    name = type->second.second;
  }
}

const XMLCh *
XQStringPool::getName(const FieldDescriptor *desc)
{
  const XMLCh *name(0);

  // search pooled name
  NameMap::const_iterator p_name = namePool_.find(desc);

  // if not set add it
  if (namePool_.end() == p_name)
  {
    if(desc){
      name = XMLString::transcode(desc->name().c_str(), memMgr_);
    } else {
      name = XMLString::transcode("_unknown_", memMgr_);
    }

    namePool_[desc] = name;
  }
  else
  {
    name = p_name->second;
  }

  return name;
}

const XMLCh *
XQStringPool::getName(const EnumValueDescriptor *desc)
{
  BOOST_ASSERT(0 != desc);

  const XMLCh *name(0);

  // search pooled name
  NameMap::const_iterator p_name = namePool_.find(desc);

  // if not set add it
  if (namePool_.end() == p_name)
  {
    name = XMLString::transcode(desc->name().c_str(), memMgr_);

    namePool_[desc] = name;
  }
  else
  {
    name = p_name->second;
  }

  return name;
}

void
XQStringPool::clear()
{
  // clear type pool
  // only second string has be freed as typeURI is always a static pointer
  for (TypeMap::iterator it = typePool_.begin(), end = typePool_.end();
       it != end; ++it)
    XMLString::release((char **)(&it->second.second), memMgr_);
  // clear name pool
  for (NameMap::iterator it = namePool_.begin(), end = namePool_.end();
       it != end; ++it)
    XMLString::release((char **)(&it->second), memMgr_);
}

}
